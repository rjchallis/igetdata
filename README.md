***any* data ● *any* device ● *any*where**
# this is igetdata

Fieldwork should be simple – go out, record observations, take measurements, collect samples. 

Of course there are usually a few challenges thrown up by the environment, terrain and weather (not to mention the need to carry specialist equipment to remote locations) but these are all part of the opportunity to be immersed in the environment your data will describe. 

Which brings us to the data themselves. The first product of a season's fieldwork should be a high quality dataset representing all the observations and measurments in a format that can be readily analysed to add to understanding of the environment they describe.

Data entry after a day or a season spent in the field raises the challenge of interpreting field maps and recording forms. Trying to decipher your own wind-assisted handwriting can be a challenge but add a team of field workers, or scale up to a citizen science project, and these problems are multiplied. Not so bad if you have a system to allow each individual to enter their own data, but persuading people who love to be in the field to then spend their evenings entering data can risk putting people off participating in the first place.

Data entry in the field offers a solution but loses one key advantage of hand written field notes - writing and drawing take very little thought away from the task in hand.  Interacting with a conventional form on a laptop, tablet or smartphone is far less intuitive and takes time and focus away from observation, especially with smaller, more portable screens. 

Of course it is possible to design an app to support a specific survey so data entry in the field can be as intuitive as writing.  Field data entry in this way has the advantage of providing an opportunity to validate entries on the fly so any mistakes can be corrected as they happen and don't leave mysteries to be resolved possibly months later when the data are collated. But with every field survey having different methods (requiring the collection of different data with different sets of possible values), finding an off-the-shelf solution is usually not an option.

**igetdata** was concieved to solve this problem. To support the collection of *any* data on *any* device, *any*where through an intuitive interface that is fully configurable to the requirments of a specific survey method through changing values in a .csv file that can be edited in any spreadsheet software.

**igetdata** is a cross-platform, mobile data-entry system designed to accomodate a wide diversity of georeferenced data. It's twin aims are to make field data entry efficient, intuitive and accurate and to make survey design straightforward and achievable without requiring specialist knowledge of interface design. It is designed to have the flexibility to support any georeferenced field data collection so if you are planning field data collection alone, in a team or as part of a major citizen science project then hopefully you will find **igetdata** useful.  


## igetdata in action
This section is intended to describe how to set up and run surveys with igetdata - if you want to know how easy it is to use in the field then you might like to scroll down to the **Field experience** section first.

### Hosting igetdata
**igetdata** uses standard web technologies (`html5`, `css3` and `javascript`) and is designed to be as independent from the internet as possible, so it really can be used anywhere. Once the survey page is open on a device, everything happens on that device until the data are ready to be uploaded. So the core files and any survey specific `.csv` files just need to be placed on a web server to be accessible to the device the survey will be undertaken on. The next time the device connects to the server will be to upload the data in `JSON` format, which can be processed into database tables or spreadsheets using any server-side scripting language. Some example scripts to handle generic data uploads will be coming soon! 

### Bring your own map
With the number of high quality mapping solutions available, the focus so far has been on implementing the unique features of **igetdata** and, so far, it will only work in the UK.  It is designed to support custom maps to provide the option for higher resolution than many of the free mapping services offer and allow survey boundaries etc. to be drawn on. If you have a map image (and the coordinates of the corners) and it's on the OS grid then you are ready to get started.  Just change a few values in `setup.csv` and your map will load ready to start collecting georefenced data. If you need to use **igetdata** outside the UK or want to see support for other mapping services then let me know and I can try to move it up the list of priorities - or better still join in with development and you can have any feature you want!
  
### <a name="points"></a>Points, lines & areas
The geographical component of a dataset can be defined as a set of points, lines and areas.  These features, and any links between them, can be added to the map with just a few taps.  A set of `.csv` files (`point.csv`, `line.csv`, `area.csv` and `link.csv`) allow you to define a different set of attributes, each with their own properties and possible values, to be associated with each feature type. 

Sometimes it will be appropriate to have one record per feature, but often it will be more useful to attach multiple records to the same feature. The option to add more records is again controlled by `.csv` file names so while `[feature name].csv` defines attributes for a single record, `[feature name]_record.csv` defines attributes for multiple records.  These two file types can be used in combination to collect data on attributes that are directly related to the feature only once while allowing multiple associated records to have a different set of attributes.

All of these files are highly configurable as described in **Defining surveys**, below.

## igetdata roadmap
**igetdata** is still under development and while there is a lot that it can do, there are still some things that it can't. There are still a few planned features/bug fixes that are needed before it's ready for serious use in the field so if you want to use **igetdata** soon then take a look at the list below and set up a test installation to see whether it seems to be suitable for your needs. Any comments you may have about your experience and suggestions for features to prioritise would be very useful. 

What has been successful so far is implementing a very simple framework for potentially complex field-survey design and a user interface to make digitisation in the field efficient and accurate. 

* Design surveys using csv files [done]
* Allow easy definition of survey area and custom base map [done]
* Add features to maps [done]
* Associate features with multiple records [done]
* Set attribute values for records [done]
* Conditional and linked attributes [done]
* Link features [done]
* Operate on any suitable device [partial - *tested on iphone & desktop, not tested on other devices, not optimised on larger screens*]
* Style/label features by attribute values [done]
* Support multiple record sets [partial - *rather basic support at present*]
* Support GPS [partial - *still to add support for storing/displaying gps track*]
* Export data [partial - *json exported but need example scripts to save to file/DB*]
* Import/export features [partial - *support for import but not export*]
* Associate images/audio with records [TODO - *access camera and microphone through HTML5, store as data uri*]
* Display scale bar and current coordinates [TODO]
* Allow user control of feature styling [TODO]
* Support additional coordinate systems [TODO - *currently only osgb support*]

## Field experience
In the field one thing counts above all others - simplicity. Complex, structured information may be essential to the goals of a project but if the resulting interface is too difficult to navigate then eventually a compromise will have to be made between time spent on the survey and time spent recording the results. A simple interface encourages complete and consistent recording throughout a survey, even when the weather turns bad. To see how simple it can be, imagine undertaking a survey with **igetdata** on your smartphone:

1. It begins with a map, use the familiar swipe and zoom controls and you're ready to go.

2. When you're ready to record an observation tap to select `draw` (with the most common shape tool highlighted) and tap again to place a `point`. Fingers can be a little clumsy on a touchscreen but not to worry, drag the point and tap it once more to move it. You can `edit` and `delete` other points just as easily too.

3. One more tap to select `data` (with the `input` option already highlighted for you) and now you can tap the point you just added to associate some values. That's just four taps and you are already editing the data record for a georeferenced point.

4. What you see next will depend on how the survey has been set up but it will always be a simple table of attributes with the most likely option already selected. Tap a cell to see the options, tap an option to set the value and if you want, tap the square to set that option as a default for new records - handy if you think you might see a lot of the same thing.

5. If it makes sense for the survey there will be a `+` symbol to add more records in the top right of the screen. Tap it to make a new record. There are two ways this can go, a new record is created with the same attributes (say another plant species in the same quadrat) or the first record can be used to hold information about the feature as a whole (like the habitat the quadrat is in) so you don't have to enter the same information in all the associated records.

6. When you're done, tap the inset map to go back to the main screen. There are a few things you may notice about your point depending on the setup. It could have a unique colour or other style applied based on one of the attributes (so you can tell at a glance what you have seen where when you've added a few more points). It could show a little number representing the number of records (maybe the number of species in your quadrat). Or it could show the actual value of one of the attributes as a label to make it very clear what your point represents. 

7. Go ahead and add another `point` then you can link them by first tapping on `link`, then tapping features to add them to your selection and tapping `join` to link them.  Once you have a set of linked points you can manage the way they are connected by chosing a point in your set and another and tapping `add` to add it or choosing a point to `unlink` from the set. When you're done tap `data` again to associate some data with the link (just as you would for more points). If you've just gone ahead and done this you may be wondering why - imagine we've seen two birds. Now birds can move around so if we know they are different (because we saw or heard them at the same time) then we can say so and it can be taken into account in subsequent analysis. If the line style has been set to depend on a link attribute (maybe solid for same and dashed for different) then we can see at a glance what the line represents.

8. If you want to add a `line` or `area`, go ahead, they work just like the points except you have to tap on one of the circular nodes when you are done so the shape will be stored and they may have a different set of attributes associated with them, whatever makes sense for the survey you are undertaking.

9. Probably best to ignore the features that are on display but haven't been implemented yet (or that are easy to work out) and take a look at how we manage the data. Tap `manage` and `reset` and you're only one more tap away from deleting all of your data irretrievably. Easy to do now but should be difficult to do accidentally. Tap `cancel` if you're attached to your data and rest assured that it a confirmation slider will be on its way to make accidental deleting even harder once development has settled down a little.

10. So far all of your records are stored in the device you recorded them on. This is good as it allows you some freedom from the internet while surveying (but you'll have to wait for a non-development release to be able to reload **igetdata** without a connection so don't take it too far from the internet just yet) but when you have a connection it would be a good idea to back up your results to a server somewhere by tapping `upload`.

11. I'm guessing it took you longer to read all that than it did to get to grips with using the interface. With **igetdata** it's easy to collect, store and view a lot of detailed, structured information on a small screen.

## Defining surveys
### [feature name].csv & [feature name]_record.csv
Simply add an individual file for each feature type you want to support so each can have unique attributes. To apply the same set of attributes across all feature types `all.csv` and `all_record.csv` can also be used in conjunction with feature-specific files. Feature types without an associated `.csv` file will not be available to the user. The complexity of these files can vary, depending on how much you need to customise the survey, and use the following headings (only the two marked with a * are essential):

  1. `attribute`\*: unique name for an attribute with no spaces, will be displayed in the record view alongside the attribute value so try to keep it short. To allow multiple selections from a common list, use sequentially numbered attribute names (as for values, see below) and higher numbered attributes will remain hidden until an option has been selected for a lower numbered attribute.
  2. `value1`\*: the first of the possible values for an attribute
  3. `value`[2..n]: subsequent possible values sequentially numbered in adjacent columns
  2. `option`[1..n]: optional values to suggest when typing
  4. `default`: set the default value by entering a `value`, `index` of position in the list of possible values, a `function` denoted by a valid function name followed by parentheses with no spaces, or the precision of a `gridref` which is a special case allowing support for 2 to 10 figure OS-style grid references. If no default is chosen the first possible `value1` is used.
  5. `lookup`: should the value be stored as an `index`, `value`, `function` or `gridref`? If present, this is used to determine the nature of the default, if absent `value` is assumed.
  5. `linked_to`: support setting of multiple attributes at once by adding linked names (seperated by semicolons) to this column, useful for setting values by code or in full or supporting both scientific and common names.
  5. `conditional_on`: format 'attribute_name=value', hide attributes unless condition is satisfied in order to show the user only attributes that make sense based on previous selection.
  6. `expanded`: by default all attributes are initially presented with options hidden in record view, enter any value in this column to force the options to be visible in the initial view.
  7. `show`: enter a value in this column to display the value of the attribute on the same row on the associated feature in map view.
  8. `style`: enter a prefix to enable custom styling of associated features by the value selected. Features will be assigned to the class prefixvalue and styles can be defined in css.
  9. `placeholder`: to allow user entries in addition to specified values, enter informative text here to use as a placeholder in a textbox that will appear at the end of the list of values in record view.
  1. `type`: if `placeholder` is defined, entering the type here (`text` or `number` will enable the appropriate input (e.g. numeric keypad) if supported on the device.
  1. `test`: if user entry is enabled, control the format by specifying a regular expression that must evaluate to true for the entry to be accepted.
  1. `limit`: an attribute such as 'count' could have many user entries and by default all will be stored, to limit the ultimate length of this list and prevent the need for excessive scrolling enter a numeric value to limit the number of entries to here, new entries beyond the `limit` will then replace the oldest existing entry.