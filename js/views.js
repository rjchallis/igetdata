      function record_view (){
        console.log(active_feature_id);
      	d3.select("#map_tools").style("height","0");
        d3.selectAll(".osdata").style("opacity","0");
        minimise_options(d3.select("#dat_tool_options"));
        d3.select("#map_tool_option_shell").style("width","0");
        d3.select("#map_tool_option_shell").style("overflow","hidden");
        //d3.selectAll(".map_tool").style("height","0");
        d3.select("#map_and_key_info").style("height","24%").style("width","100%");
        //d3.select("#key_info").style("width","100%");
        d3.select("#map_container").style("width","32%");
        d3.select("#attribute_container").style("height","76%");
        center_map(geo_features[active_feature_id].x,geo_features[active_feature_id].y)
        map_div.style("height","100%");
        svg.attr("height", map_div.style("height"));
        svg.attr("width", map_div.style("width"));
        //svg.attr("preserveAspectRatio","xMidYMid meet");
        map_cover.attr("fill","white");
        if (geo_features[active_feature_id].records.length == 0){
            create_new_record(active_feature_id);
        }
        show_feature_detail(active_feature_id);
      }
      function map_view (){
      	console.log("here");
      	d3.select("#map_tools").style("height","3em");
        d3.selectAll(".osdata").style("opacity","1");
        d3.select("#map_tool_option_shell").style("width","100%");
        d3.select("#map_tool_option_shell").style("overflow","visible");
        //d3.selectAll(".map_tool").style("height","1.5em");
        d3.select("#map_and_key_info").style("height","100%");
        d3.select("#map_container").style("width","100%");
        d3.select("#attribute_container").style("height","0%");
        map_div.style("height","");
        svg.attr("width", map_div.style("width"));
        svg.attr("height", map_div.style("height"));
        svg.attr("viewBox", vb_arr[0]+" "+vb_arr[1]+" "+vb_arr[2]+" "+vb_arr[3]);
        map_cover.attr("fill","none");
        new_feature_group.selectAll("circle").remove();
      	new_feature_group.selectAll("polygon").attr("points","");
      	new_feature_group.selectAll("polyline").attr("points","");
      	my_poly_points = [];
      	localStorage.setItem("active_records",JSON.stringify(active_records));
      	localStorage.setItem("geo_features",JSON.stringify(geo_features));
      	draw_nodes();
      	active_feature_id = null;
        draw_features();
      }
      

