window.onload = initialise_survey;

window.addEventListener('load', function() {
    FastClick.attach(document.body);
}, false);


var width = 400000,
    height = 500000,
    ll_E,
    ll_N,
    size,
    resolution,
    view_GR,
    map_GR,
    map_height,
    map_width,
    map_ll_E,
    map_ll_N,
    map_file,
    min_zoom,
    max_zoom;

var current_set_id;
var feature_sets = [];
var stored_ids = {};
var active_records = {};
var features = {"polygons":{},"points":{},"lines":{},"links":{}};
var ids = {};
function today() { var ret = get_date_time("ddmmyyyy","/"); return ret;}
function this_year() { var ret = get_date_time("yyyy"); return ret;}
function now(){ var ret = get_date_time("hhmmss",":"); return ret;}

var linked_attributes = {	"species_name":["scientific_name","sp_code"]};

//set up regular expressions to test values
function re_ddmmyyyy(){ var patt=  /^[0123]\d\/[01][\d]\/[12][\d]{3}$/; return patt;} // contains a millenium bug!
function  re_time(){ var patt=  /^[012]\d:[012345]\d$/; return patt;}
function  re_char_space(){ var patt=  /^[\w\s]+$/; return patt;}
function  re_char (){ var patt=  /^[\w]+$/; return patt;}
function  re_positive_integer(){ var patt=  /^[\d]+$/; return patt;}
//control display settings for each user attribute
// can use css_class "expanded" but harms user interface if lots of attributes
function get_date_time (format,sep){
	// only a very limited set of formats are currently supported
	var my_date_time = new Date();
	if (format == "ddmmyyyy"){
		var dd = my_date_time.getDate();
		var mm = my_date_time.getMonth()+1; //January is 0!
		var yyyy = my_date_time.getFullYear();
		if(dd<10){dd='0'+dd} 
		if(mm<10){mm='0'+mm} 
		my_date_time = dd+sep+mm+sep+yyyy;
		return my_date_time;
	}
	else if (format == "yyyy"){
		var yyyy = my_date_time.getFullYear();
		my_date_time = yyyy;
		return my_date_time;
	}
	else {
	var hh = my_date_time.getHours();
	var mm = my_date_time.getMinutes();
	//var ss = my_date_time.getSeconds();
	if(hh<10){hh='0'+hh} 
	if(mm<10){mm='0'+mm} 
	//if(ss<10){ss='0'+ss} 
	my_date_time = hh+sep+mm;
	return my_date_time;
	}
}

  function current_grid_ref(figures){
  	return "-";
  }


 var string_to_function = function(function_as_string){
 	//console.log(function_as_string);
    var fn = window[function_as_string];
    return fn();
};

function fileExists(url)
{
    var http = new XMLHttpRequest();
    http.open('HEAD', url, false);
    http.send();
    return http.status!=404;
    
}

var user_attributes = {};
var attribute_object = {};
var geo_features = {};
stored_ids = {};
stored_features = {};


function initialise_survey(){


				localStorage.clear();
if (!localStorage.getItem("user_attributes")){



// set parameters from setup.csv
var path = "csv/setup.csv";
//console.log(path);
if (!fileExists(path)) {
	//console.log("no file at "+path);
}
else {
	console.log(path);
	d3.csv(path, function(error, data) {
		if (error) return;
		//console.log(data);
		data.forEach(function(d) {
			if (d.parameter == "view_size"){
				size = d.value;
			}
			else if (d.parameter == "resolution"){
				resolution = d.value;
			}
			else if (d.parameter == "view_GR"){
				//console.log(d.value)
				view_GR = OsGridRef.parse(d.value);
				//console.log(view_GR);
				ll_E = view_GR.easting;
				ll_N = view_GR.northing;
			}
			else if (d.parameter == "map_GR"){
				map_GR = OsGridRef.parse(d.value);
				map_ll_E = map_GR.easting;
				map_ll_N = map_GR.northing;
			}
			else if (d.parameter == "map_width"){
				map_width = d.value;
			}
			else if (d.parameter == "map_height"){
				map_height = d.value;
			}
			else if (d.parameter == "min_zoom"){
				min_zoom = d.value;
			}
			else if (d.parameter == "max_zoom"){
				max_zoom = d.value;
			}
			else if (d.parameter == "map_file"){
				map_file = d.value;
			}
		});
	});
}
// set served features from features.csv
var path = "csv/features.csv";
//console.log(path);
var served_sets = {};
if (!fileExists(path)) {
	//console.log("no file at "+path);
}
else {
	d3.csv(path, function(error, data) {
		if (error) return;
		//attribute_object[type] = {}
		//user_attributes[type] = [];
		//if (user_attributes["all"] && (type == "area" || type == "line" || type == "point")){
		//	attribute_object[type] = JSON.parse(JSON.stringify(attribute_object["all"]));
		//	user_attributes[type] = user_attributes["all"].slice(0);
			data.forEach(function(d) {
				//console.log(d.id);
				//console.log(d.type);
				features[d.type][d.id] = {};
				features[d.type][d.id]["id"] = d.id;
				var my_points = d.points.split(" ");
				features[d.type][d.id]["points"] = [];
				my_points.forEach(function(GR){
					my_gridref = OsGridRef.parse(GR);
        			features[d.type][d.id]["points"].push([my_gridref.easting,height-my_gridref.northing]);
        		});
        		features[d.type][d.id]["set_id"] = d.set_id;
        		if (d.retain){
        			features[d.type][d.id]["retain"] = d.retain;
        		}
        		if (d.noselect){
        			features[d.type][d.id]["noselect"] = d.noselect;
        		}
        		stored_ids[d.id] = d.type;
        		stored_features[d.id] = features[d.type][d.id];
        		if (!served_sets[d.set_id]){
        			feature_sets.push(d.set_id);
        			served_sets[d.set_id] = 1;
        		}
			});
		});
	
	setTimeout(function(){localStorage.setItem("ids",JSON.stringify(stored_ids));},300);
	setTimeout(function(){localStorage.setItem("features",JSON.stringify(stored_features));},300);
}



// setup survey from remaining csv files
var file_types = ["all","all_record","area","area_record","gps","line","line_record","point","point_record","link","link_record"];
file_types.forEach(function(type){
	path = "csv/"+type+".csv";
	//console.log(path);
	if (!fileExists(path)) {
	//	console.log("no file at "+path);
	}
	else {
		//console.log(path);
		d3.csv(path, function(error, data) {
			if (error) return;
			attribute_object[type] = {}
			user_attributes[type] = [];
			if (user_attributes["all"] && (type == "area" || type == "line" || type == "point")){
				attribute_object[type] = JSON.parse(JSON.stringify(attribute_object["all"]));
				user_attributes[type] = user_attributes["all"].slice(0);
			}
			if (user_attributes["all_record"] && (type == "area_record" || type == "line_record" || type == "point_record")){
				attribute_object[type] = JSON.parse(JSON.stringify(attribute_object["all_record"]));
				user_attributes[type] = user_attributes["all_record"].slice(0);
			}
			var mappedArray = d3.entries(data[0]);
			var valueKey = mappedArray[1].key;
			geo_features[type] = {};
			if (!geo_features[type.replace("_record","")]){
				geo_features[type.replace("_record","")] = {};
			}
			data.forEach(function(d) {
				d.value = d[valueKey];
				user_attributes[type].push(d.attribute);
				var v = 1;
				attribute_object[type][d.attribute] = {};
				attribute_object[type][d.attribute].values = [];
				while (d["value"+v]){
					if (d["value"+v].match("\\(\\)")){
						var val = string_to_function(d["value"+v].replace("()",""));
						attribute_object[type][d.attribute].values.push(val);
					}
					else {
						attribute_object[type][d.attribute].values.push(d["value"+v]);
					}
					v++;
				}
				if (d.lookup){
					attribute_object[type][d.attribute].lookup = d.lookup;
				}
				else {
					attribute_object[type][d.attribute].lookup = "value";
				}
				if (d.conditional_on){
					attribute_object[type][d.attribute].conditional_on = [];
					attribute_object[type][d.attribute].conditional_value = [];
					var conditions = d.conditional_on.split("|");
					conditions.forEach(function(condition){
						var conditional = condition.split("=");
						attribute_object[type][d.attribute].conditional_on.push(conditional[0]);
						attribute_object[type][d.attribute].conditional_value.push(conditional[1]);
					});
				}
				if (d.linked_to){
					attribute_object[type][d.attribute].linked_to = d.linked_to;
				}
				if (d.test){
					attribute_object[type][d.attribute].test = new RegExp(d.test);
				}
				if (d.type){
					attribute_object[type][d.attribute].type = d.type;
				}
				else if (d.placeholder){
					attribute_object[type][d.attribute].type = "text";
				}
				if (d.expanded){
					attribute_object[type][d.attribute].css_class = "expanded";
				}
				else {
					attribute_object[type][d.attribute].css_class = "collapsed";
				}
				if (d.placeholder){
					attribute_object[type][d.attribute].placeholder = d.placeholder;
					var o = 1;
					if (d["option"+o]){
						attribute_object[type][d.attribute].options = [];
					}
					while (d["option"+o]){
						if (d["option"+o].match("\\(\\)")){
							var val = string_to_function(d["option"+o].replace("()",""));
							attribute_object[type][d.attribute].options.push(val);
						}
						else {
							attribute_object[type][d.attribute].options.push(d["option"+o]);
						}
						o++;
					}
				}
				if (!d["default"]){
					if (d.lookup){
						if (d.lookup == "index"){
							d["default"] = 0;
						}
						else if (d.lookup == "gridref"){
							d["default"] = 6;
						}
						else {
							d["default"] = d.value1;
						}
					}
					else {
						d.lookup = "value";
						d["default"] = d.value1;
					}
				}
				else {
					attribute_object[type][d.attribute]["default"] = d["default"];
					attribute_object[type][d.attribute]["userdefault"] = d["default"];
					//console.log(d["default"]);
				}
				if (d.limit){
					attribute_object[type][d.attribute].limit = d.limit;
				}
				if (d.style){
					geo_features[type].style_by = d.attribute;
					geo_features[type].style_prefix = d.style;
				}
				if (d.show){
					geo_features[type.replace("_record","")].show = d.attribute;
				}
			});
		});

	}
});
current_set_id = timestamp();
feature_sets.push(current_set_id);

setTimeout(function(){localStorage.setItem("user_attributes",JSON.stringify(user_attributes))},300);
setTimeout(function(){localStorage.setItem("attribute_object",JSON.stringify(attribute_object))},300);
setTimeout(function(){localStorage.setItem("geo_features",JSON.stringify(geo_features))},300);
setTimeout(function(){localStorage.setItem("feature_sets",JSON.stringify(feature_sets))},300);
setTimeout(function(){localStorage.setItem("active_records",JSON.stringify(active_records))},300);
setTimeout(function(){localStorage.setItem("ll_E",ll_E);
localStorage.setItem("ll_N",ll_N);
localStorage.setItem("size",size);
localStorage.setItem("resolution",resolution);
localStorage.setItem("map_height",map_height);
localStorage.setItem("map_width",map_width);
localStorage.setItem("map_ll_E",map_ll_E);
localStorage.setItem("map_ll_N",map_ll_N);
localStorage.setItem("map_file",map_file);
localStorage.setItem("min_zoom",min_zoom);
localStorage.setItem("max_zoom",max_zoom);},300);
}
else {

    
	user_attributes = JSON.parse(localStorage.getItem("user_attributes"));
	attribute_object = JSON.parse(localStorage.getItem("attribute_object"));
	geo_features = JSON.parse(localStorage.getItem("geo_features"));
	feature_sets = JSON.parse(localStorage.getItem("feature_sets"));
	current_set_id = feature_sets[(feature_sets.length-1)];
	//console.log(feature_sets);
	//console.log("k");
	//console.log(geo_features);
ll_E = localStorage.getItem("ll_E");
ll_N = localStorage.getItem("ll_N");
size = localStorage.getItem("size");
resolution = localStorage.getItem("resolution");
map_height = localStorage.getItem("map_height");
map_width = localStorage.getItem("map_width");
map_ll_E = localStorage.getItem("map_ll_E");
map_ll_N = localStorage.getItem("map_ll_N");
map_file = localStorage.getItem("map_file");
min_zoom = localStorage.getItem("min_zoom");
max_zoom = localStorage.getItem("max_zoom");
feature_sets = JSON.parse(localStorage.getItem("feature_sets"));
stored_features = JSON.parse(localStorage.getItem("features"));
stored_ids = JSON.parse(localStorage.getItem("ids"));
active_records = JSON.parse(localStorage.getItem("active_records"));
}

for (var my_id in stored_ids){
	if (stored_ids[my_id] == "points"){
		features.points[my_id] = stored_features[my_id];
	}
	if (stored_ids[my_id] == "lines"){
		features.lines[my_id] = stored_features[my_id];
	}
	if (stored_ids[my_id] == "polygons"){
		features.polygons[my_id] = stored_features[my_id];
	}
	if (stored_ids[my_id] == "links"){
		features.links[my_id] = stored_features[my_id];
	}
}
////active_records = JSON.parse(localStorage.getItem("records"));

//console.log(stored_ids);
//console.log(features.polygons);
////console.log(active_records);
//setTimeout(function(){console.log(features.links)},500);
//
//features.points = JSON.parse(localStorage.getItem("points"));
//features.lines = JSON.parse(localStorage.lines)[0];
//features.polygons = JSON.parse(localStorage.polygons)[0];
//features.links = JSON.parse(localStorage.links)[0];
setTimeout(function(){draw_map()},500);
}