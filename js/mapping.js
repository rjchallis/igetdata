
var vb_arr;
var map_div,
      svg,
      main_group,
        img,
        guide_group,
	    feature_group,
          polygon_group,
          line_group,
          point_group,
          link_group,
        multi_feature_group,
          multi_polygon_group,
          multi_line_group,
          multi_point_group,
        new_feature_group,
          new_feature_rect,
        gps_position_group,
          gps_position_symbol,
        multi_select_group,
          multi_select_draw_group,
          multi_select_interact_rect,
        pan_zoom_group,
          pan_zoom_rect,
        map_cover_group,
          map_cover;
    	
    	
		var multi_features = {"polygons":{},"points":{},"lines":{},"links":{}};
		var points;
      var compass_tool;
      var shapes_tool;
      var pan_zoom_tool;
      var active_feature_id;
      var select_tool_shell;
       var select_tool_options;
       var select_tool;
       var confirmation_screen;
       var current_tool_div;
       var link_features = [];
       
    
 /*   var zoom = d3.behavior.zoom()
    .scaleExtent([1, 10])
    .on("zoom", zoomed);
        function zoomed() {
        tmp_vb_arr[0] = vb_arr[0] - d3.event.translate[0];
        tmp_vb_arr[1] = vb_arr[1] - d3.event.translate[1];
      // //console.log(tmp_vb_arr);
  svg.attr("viewBox", tmp_vb_arr[0]+" "+tmp_vb_arr[1]+" "+tmp_vb_arr[2]+" "+tmp_vb_arr[3]);//, 
 // main_group.attr("transform","scale(" + d3.event.scale/2 + ")");
    main_group.transition().duration(100).attr("transform", "translate(" + d3.event.translate + ")scale(" + d3.event.scale + ")");

}*/


var Maptool = function(name,prefix,options,preset){
  this.name = name;
  this.prefix = prefix;
  this.options = options;
  this.preset = preset;
  this.status = 'off';
	Maptool.objects.push(this);
}
Maptool.objects = [];


function add_tool(tool_div,options_div,tool){
	var new_tool = tool_div.append("div").attr("id",tool.prefix+"tool").attr("class","map_tool inactive_tool");
	var tool_options_div = options_div.append("div").attr("id",tool.prefix+"tool_options").attr("class","map_tool_options");
    make_options(tool_options_div,tool);
    new_tool
    	.text(tool.name)
    	.on("click",function(){
    			if (tool.status == 'off'){
    				// turn off other map tools
    				Maptool.objects.forEach(function(a_tool){
    					a_tool.status = 'off';
    					d3.select("#"+a_tool.prefix+"tool").attr("class","map_tool inactive_tool");
    					d3.select("#"+a_tool.prefix+"tool_options").attr("class","map_tool_options");
    				});
    				tool.status = 'on';
    				new_tool.attr("class","map_tool active_tool");
    				if (!tool.preset || tool.preset == null){
    					tool_options_div.selectAll(".map_tool_single_option").attr("class","map_tool_single_option");
    				}
    				tool_options_div.attr("class","map_tool_show_options");
    				tool_change(tool_options_div,tool.prefix,tool.preset,'on');
    				over_div.attr("class","overlay_hidden");
    			}
    			else if (tool.status == 'on'){
    				tool.status = 'off';
    				//console.log(tool.status);
    				tool_options_div.attr("class","map_tool_options");
    				current_tool_div.attr("class","hide_current");
    				new_tool.attr("class","map_tool inactive_tool");
    				tool_change(tool_options_div,tool.prefix,tool.preset,'off');
    				over_div.attr("class","overlay_show");
    
    			}
    			//console.log(tool_options_div.attr("id"));
    		});
}   


var make_options = function(opt_div,tool){
	var option_width = 100 / tool.options.length;
	option_width += "%";
	tool.options.forEach(function(option){
		opt_div.append("div")
			.attr("id",tool.prefix+option)
			.attr("class",function(){var my_class = "map_tool_single_option"; if (option == tool.preset){my_class += " selected_tool"}return my_class})
			.style("width",option_width)
			.text(option)
			.on("click",function(){
				d3.select(this).attr("class",function(){var my_class = "map_tool_single_option"; if (tool.preset && tool.preset != null  && option != "sets" && option != "style"){tool.preset = option; }my_class += " selected_tool";d3.select(this.parentNode).selectAll(".selected_tool").attr("class","map_tool_single_option");return my_class});
				tool_change(opt_div,tool.prefix,option,'on');
			});
		});
}

function minimise_options (opt_div){
	opt_div.attr("class","map_tool_options");
    current_tool_div.attr("class","show_current").on("click",function(){opt_div.attr("class","map_tool_show_options")});
}

function tool_change(opt_div,prefix,option,toggle){
	//console.log(prefix+option);
	//console.log(opt_div);
	current_tool = option;	
	current_tool_div.text(option);	
	over_div.attr("class","overlay_hidden");
	if(prefix == "drw_"){
		
        active_feature_id = undefined;
		new_feature_group.selectAll("circle").remove();
		new_feature_group.selectAll("polygon").attr("points","");
		new_feature_group.selectAll("polyline").attr("points","");
		pan_zoom_rect.style("pointer-events","none");
		my_poly_points = [];
		draw_nodes();
		multi_features.points = {};
		multi_features.lines = {};
		multi_features.polygons = {};
		draw_features(1);
		if (toggle == 'off' || option == 'edit' || option == 'delete'){
			new_feature_group.style("pointer-events","none");
		}
		else {
			new_feature_group.style("pointer-events","auto")
			.on("click",function(){minimise_options(opt_div)});
			if (option == "point"){
				new_feature_rect.on("click",draw_point);
				//console.log("point")
			
			}
			else if (option == "line"){
				new_feature_rect.on("click",start_line);
				//console.log("line")
			
			}
			else if (option == "area"){
				new_feature_rect.on("click",start_area);
				//console.log("area")
			
			}
		}
		if (option == "edit"){
      		
		}
		else if (option == "delete"){
      		
		}
	}
	else if(prefix == "dat_"){
		new_feature_group.selectAll("circle").remove();
		new_feature_group.selectAll("polygon").attr("points","");
		new_feature_group.selectAll("polyline").attr("points","");
		pan_zoom_rect.style("pointer-events","none");
		new_feature_group.style("pointer-events","none");
			my_poly_points = [];
		if (option == "input"){
			draw_nodes();
		
			multi_features.points = {};
			multi_features.lines = {};
			multi_features.polygons = {};
			draw_features(1);
      		if (toggle == 'off'){
      			
      		}
		}
		else if (option == "sets"){
      		// manage feature and record sets to reduce display clogging
      		manage_sets();
      		
		}
		
		else if (option == "style"){
      		unsupported();
			
		}
		
		
		
	}
	else if(prefix == "lnk_"){
		new_feature_group.selectAll("circle").remove();
		new_feature_group.selectAll("polygon").attr("points","");
		new_feature_group.selectAll("polyline").attr("points","");
		pan_zoom_rect.style("pointer-events","none");
		new_feature_group.style("pointer-events","none");
			my_poly_points = [];
			draw_nodes();
		if (option == "join"){
			link_selected_features();
			opt_div.selectAll(".selected_tool").attr("class","map_tool_single_option");
			tool_change(opt_div,prefix,undefined,toggle);
		}
		else if (option == "add"){
			link_selected_features(1);
			opt_div.selectAll(".selected_tool").attr("class","map_tool_single_option");
			tool_change(opt_div,prefix,undefined,toggle);
		}
		else if (option == "unlink"){
			unlink_selected_features();
			opt_div.selectAll(".selected_tool").attr("class","map_tool_single_option");
			tool_change(opt_div,prefix,undefined,toggle);
		}
		
		
	}

	else if(prefix == "gps_"){
		new_feature_group.selectAll("circle").remove();
		new_feature_group.selectAll("polygon").attr("points","");
		new_feature_group.selectAll("polyline").attr("points","");
		pan_zoom_rect.style("pointer-events","none");
		new_feature_group.style("pointer-events","none");
		my_poly_points = [];
		draw_nodes();
		multi_features.points = {};
		multi_features.lines = {};
		multi_features.polygons = {};
		draw_features(1);
		if (option == "on"){
			getLocation("start")
		}
		else if (option == "off"){
			if (gps){
				getLocation("stop")
			}
		}
		else if (option == "options"){
      		unsupported();
			
		}
	}
	else if(prefix == "man_"){
		new_feature_group.selectAll("circle").remove();
		new_feature_group.selectAll("polygon").attr("points","");
		new_feature_group.selectAll("polyline").attr("points","");
		pan_zoom_rect.style("pointer-events","none");
		new_feature_group.style("pointer-events","none");
		my_poly_points = [];
		draw_nodes();
		// possibly extend to allow uploading selected points or deleting selected points?
		multi_features.points = {};
		multi_features.lines = {};
		multi_features.polygons = {};
		draw_features(1);
		if (option == "upload"){
      		// save records to remote server
      		manage_data_upload();
      		
		}
		else if (option == "views"){
      		unsupported();
			
		}
		else if (option == "reset"){
      		//clear local storage and reload page
      		manage_reset();
		}
	}
}

function confirm_delete(records,object,id){
	var caution_note = records.length + " associated records";
	if (records.length == 1){
		caution_note = records.length + " associated record";
	}
	else if (records.length == 0){
		caution_note = "no associated records";
	}
	var conf_div = confirmation_screen.attr("class","conf_show").append("div").attr("class","confirm_text_box")
	conf_div.text("This feature has "+caution_note+". Are you sure you want to delete?");
	conf_div.append("br");
	conf_div.append("div")
		.attr("class","deny_button")
		.text("no")
		.on("click",function(){
				confirmation_screen.html("").attr("class","conf_hide");
				new_feature_group.selectAll("circle").remove();
				new_feature_group.selectAll("polygon").attr("points","");
				new_feature_group.selectAll("polyline").attr("points","");
				my_poly_points = [];
				draw_nodes();
			});
	conf_div.append("div")
		.attr("class","confirm_button")
		.text("yes")
		.on("click",function(){
				confirmation_screen
					.html("")
					.attr("class","conf_hide");
				records.forEach(function(record){
					delete active_records[record];
					//console.log(record);
					update_record_store(record);
					
				});
				
				new_feature_group.selectAll("circle").remove();
				new_feature_group.selectAll("polygon").attr("points","");
				new_feature_group.selectAll("polyline").attr("points","");
				my_poly_points = [];
				draw_nodes();
				delete object[id];
				unstore_feature(id);
				draw_features();
			});

}

function manage_data_upload(){
	var conf_div = confirmation_screen.attr("class","conf_show").append("div").attr("class","confirm_text_box")
	conf_div.text("Preparing to upload records");
	conf_div.append("br");
	conf_div.append("div")
		.attr("class","deny_button")
		.text("cancel")
		.on("click",function(){confirmation_screen.html("").attr("class","conf_hide");});
	conf_div.append("div")
		.attr("class","confirm_button")
		.text("upload")
		.on("click",function(){
				
				
      			d3.html("http://192.168.1.65/cgi-bin/test/test.pl")
    			.header("Content-Type", "application/json")
    			.post(JSON.stringify({"features":features,"records":active_records}), function(error, data) {
    			  // callback
    			  	if (error){
    			  		conf_div.text("Upload failed");
						conf_div.append("br");
						conf_div.append("div")
							.attr("class","deny_button")
							.text("cancel")
							.on("click",function(){confirmation_screen.html("").attr("class","conf_hide");});
						conf_div.append("div")
							.attr("class","confirm_button")
							.text("try again")
							.on("click",function(){confirmation_screen.html("");manage_data_upload()});
    			  	}
    			  	else {
    			  	conf_div.text("Upload successful");
					conf_div.append("br");
					conf_div.append("div")
						.attr("class","confirm_button")
						.text("dismiss")
						.on("click",function(){confirmation_screen.html("").attr("class","conf_hide");});
					}
    			});
			});
}
function manage_sets(){
	var conf_div = confirmation_screen.attr("class","conf_show").append("div").attr("class","confirm_text_box")
	conf_div.text("Choose record set to display:");
	// dropdown box here
	conf_div.append("div").text("all")
		.on("click",function(){
				show_hide_features(0);
			});
	feature_sets.forEach(function(set_id){
		conf_div.append("div").text(set_id)
			.on("click",function(){
				current_set_id = set_id;
				show_hide_features(0,current_set_id);
			});
		})
	
	conf_div.append("div")
		.attr("class","deny_button")
		.text("close")
		.on("click",function(){
			console.log(features.points);
				confirmation_screen.html("").attr("class","conf_hide");
				// loop through all features and make hidden=TRUE
				// show_hide_features(0,current_set_id)
			});
	conf_div.append("div")
		.attr("class","confirm_button")
		.text("new")
		.on("click",function(){
				confirmation_screen.html("").attr("class","conf_hide");
				// loop through all features and make hidden=FALSE
				current_set_id = timestamp();
				feature_sets.push(current_set_id);
				show_hide_features(1,current_set_id,1);
				localStorage.setItem("feature_sets",JSON.stringify(feature_sets));
			});
}

function show_hide_features(hide,set_id,new_set){
	for (var key in features.points) {
  		if (features.points.hasOwnProperty(key)) {
  			//console.log(set_id);
  			//console.log(features.points[key]["set_id"]);
  			if (hide || (set_id && set_id != features.points[key].set_id)){
	  			features.points[key].hidden = true;
	  		}
	  		else {
	  			features.points[key].hidden = false;
	  		}
	  		if (new_set && features.points[key].retain){
	  			var new_key = key+"_"+timestamp();
	  			features.points[new_key] = {};
	  			for (var item in features.points[key]) {
  					if (features.points[key].hasOwnProperty(item)) {
	  					features.points[new_key][item] = features.points[key][item];
	  				}
	  			}
	  			features.points[new_key].id = new_key;
	  			features.points[new_key].hidden = false;
	  			features.points[new_key].retain = false;
	  			features.points[new_key].set_id = set_id;
	  			store_feature(new_key,"points");
	  		}
			update_feature(key,"points");
  		}	
  	}
  	for (var key in features.polygons) {
  		if (features.polygons.hasOwnProperty(key)) {
  			if (hide || (set_id && set_id != features.polygons[key].set_id)){
	  			features.polygons[key].hidden = true;
	  		}
	  		else {
	  			features.polygons[key].hidden = false;
	  		}
	  		if (new_set && features.polygons[key].retain){
	  			var new_key = key+"_"+timestamp();
	  			features.polygons[new_key] = {};
	  			for (var item in features.polygons[key]) {
  					if (features.polygons[key].hasOwnProperty(item)) {
	  					features.polygons[new_key][item] = features.polygons[key][item];
	  				}
	  			}
	  			features.polygons[new_key].id = new_key;
	  			features.polygons[new_key].hidden = false;
	  			features.polygons[new_key].retain = false;
	  			features.polygons[new_key].set_id = set_id;
	  			store_feature(new_key,"polygons");
	  		}
			update_feature(key,"polygons");
  		}	
  	}
  	for (var key in features.lines) {
  		if (features.lines.hasOwnProperty(key)) {
  			if (hide || (set_id && set_id != features.lines[key].set_id)){
	  			features.lines[key].hidden = true;
	  		}
	  		else {
	  			features.lines[key].hidden = false;
	  		}
	  		if (new_set && features.lines[key].retain){
	  			var new_key = key+"_"+timestamp();
	  			features.lines[new_key] = {};
	  			for (var item in features.lines[key]) {
  					if (features.lines[key].hasOwnProperty(item)) {
	  					features.lines[new_key][item] = features.lines[key][item];
	  				}
	  			}
	  			features.lines[new_key].id = new_key;
	  			features.lines[new_key].hidden = false;
	  			features.lines[new_key].retain = false;
	  			features.lines[new_key].set_id = set_id;
	  			store_feature(new_key,"lines");
	  		}
			update_feature(key,"lines");
  		}	
  	}
  	for (var key in features.links) {
  		if (features.links.hasOwnProperty(key)) {
  			if (hide || (set_id && set_id != features.links[key].set_id)){
	  			features.links[key].hidden = true;
	  		}
	  		else {
	  			features.links[key].hidden = false;
	  		}
	  		if (new_set && features.links[key].retain){
	  			var new_key = key+"_"+timestamp();
	  			features.links[new_key] = {};
	  			for (var item in features.links[key]) {
  					if (features.links[key].hasOwnProperty(item)) {
	  					features.links[new_key][item] = features.links[key][item];
	  				}
	  			}
	  			features.links[new_key].id = new_key;
	  			features.links[new_key].hidden = false;
	  			features.links[new_key].retain = false;
	  			features.links[new_key].set_id = set_id;
	  			store_feature(new_key,"links");
	  		}
			update_feature(key,"links");
  		}	
  	}draw_features();
}

function manage_reset(){
	var conf_div = confirmation_screen.attr("class","conf_show").append("div").attr("class","confirm_text_box")
	conf_div.text("Warning this will clear all records and cannot be undone.");
	conf_div.append("br");
	conf_div.append("div")
		.attr("class","deny_button")
		.text("cancel")
		.on("click",function(){confirmation_screen.html("").attr("class","conf_hide");});
	conf_div.append("div")
		.attr("class","confirm_button")
		.text("reset")
		.on("click",function(){
				confirmation_screen
					.html("")
					.attr("class","conf_hide");
				localStorage.clear();
      			location.reload(0);
			});
}

function unsupported(){
	var conf_div = confirmation_screen.attr("class","conf_show").append("div").attr("class","confirm_text_box")
	conf_div.text("Sorry, this option is not supported yet.");
	conf_div.append("br");
	conf_div.append("div")
		.attr("class","confirm_button")
		.text("dismiss")
		.on("click",function(){
				confirmation_screen
					.html("")
					.attr("class","conf_hide");
			});
}


function store_feature (my_id,type){
        	//stored_ids = JSON.parse(localStorage.getItem("ids"));
        	console.log(my_id);
			console.log(type);
			console.log(stored_ids);
        	stored_ids[my_id] = type;
        	localStorage.setItem("ids",JSON.stringify(stored_ids));
        	//stored_features = JSON.parse(localStorage.getItem("features"));
        	stored_features[my_id] = features[type][my_id];
        	localStorage.setItem("features",JSON.stringify(stored_features));
        	localStorage.setItem("geo_features",JSON.stringify(geo_features));
}

function update_feature (my_id,type){
        	stored_features = JSON.parse(localStorage.getItem("features"));
        	stored_features[my_id] = features[type][my_id];
        	localStorage.setItem("features",JSON.stringify(stored_features));
        	localStorage.setItem("geo_features",JSON.stringify(geo_features));
}


function unstore_feature (my_id){
			stored_ids = JSON.parse(localStorage.getItem("ids"));
        	delete stored_ids[my_id];
        	localStorage.setItem("ids",JSON.stringify(stored_ids));
        	stored_features = JSON.parse(localStorage.getItem("features"));
        	delete stored_features[my_id];
        	localStorage.setItem("features",JSON.stringify(stored_features));
        	delete geo_features[my_id];
        	localStorage.setItem("geo_features",JSON.stringify(geo_features));
}
function update_record_store (){
			localStorage.setItem("records",JSON.stringify(active_records));
}

function draw_map() {
                  d3.select("body").on("touchstart",function(d){
        			preventDefault();
        		});
	//os_transform = function([x, y]) {
    //   return [x, height-y];
    // };
    var current_tool = "";
    vb_arr = [ll_E,(height-size-ll_N),size,size];
    tmp_vb_arr = vb_arr.slice(0);
    map_div = d3.select("#map");
    //over_div = d3.select("#map_overlay");
    svg = map_div.append("svg")
        .attr("width", map_div.property("offsetWidth"))
        .attr("height", map_div.property("offsetHeight"))
        .attr("viewBox", vb_arr[0]+" "+vb_arr[1]+" "+vb_arr[2]+" "+vb_arr[3])
        .attr("preserveAspectRatio","xMidYMid meet");
    main_group = svg.append("g").attr("id","main_group");
    main_group.attr("transform","rotate(0)");
    img = main_group.append("image")
      	.attr("id","img_group")
    	.attr("xlink:href",map_file)
    	.attr("x",map_ll_E)
    	.attr("y",height-map_height-map_ll_N)
    	.attr("height",map_height)
    	.attr("width",map_width);
    over_div = map_div.append("div")
    	.attr("id","map_overlay")
    	.attr("class","overlay_show");
    vis = over_div.append("svg");
    vis
        .attr("width", map_div.property("offsetWidth"))
        .attr("height", map_div.property("offsetHeight"))
        .attr("viewBox", vb_arr[0]+" "+vb_arr[1]+" "+vb_arr[2]+" "+vb_arr[3])
        .attr("preserveAspectRatio","xMidYMid meet");
    move_group = vis.append("g").attr("id","move_group");
    move_group.attr("transform","rotate(0)");
    move_rect = move_group.append("rect").attr("id","move_rect");
    move_rect
    	.attr("x",0)
    	.attr("y",0)
   		.attr("width",width)
    	.attr("height",height)
    	.attr("fill","white")
    	.style("opacity","0")
    	.call(d3.behavior.zoom().scaleExtent([min_zoom, max_zoom]).on("zoom", zoom));
    guide_group = main_group.append("g").attr("id","guide_group");
    feature_group = main_group.append("g").attr("id","feature_group");
    polygon_group = feature_group.append("g").attr("id","polygon_group");
    line_group = feature_group.append("g").attr("id","line_group");
    point_group = feature_group.append("g").attr("id","point_group");
    link_group = feature_group.append("g").attr("id","link_group");
    multi_feature_group = main_group.append("g").attr("id","multi_feature_group");
    multi_polygon_group = multi_feature_group.append("g").attr("id","multi_polygon_group");
    multi_line_group = multi_feature_group.append("g").attr("id","multi_line_group");
    multi_point_group = multi_feature_group.append("g").attr("id","multi_point_group");
    new_feature_group = main_group.append("g").attr("id","new_feature_group");
    new_feature_rect = new_feature_group.append("rect").attr("id","new_feature_rect");
    gps_position_group = main_group.append("g").attr("id","gps_position_group");
    gps_position_symbol = gps_position_group.append("circle").attr("id","gps_position_symbol");
    multi_select_group = main_group.append("g").attr("id","multi_select_group");
    multi_select_draw_group = multi_select_group.append("g").attr("id","multi_select_draw_group");
    multi_select_interact_rect = multi_select_group.append("rect").attr("id","multi_select_interact_rect");
    pan_zoom_group = main_group.append("g").attr("id","pan_zoom_group");
    pan_zoom_rect = pan_zoom_group.append("rect").attr("id","pan_zoom_rect");
    map_cover_group = main_group.append("g").attr("id","map_cover_group");
    map_cover = map_cover_group.append("rect").attr("id","map_cover");
    gps_position_symbol
		.attr("cx",0)
    	.attr("cy",0)
   		.attr("r",resolution/2)
    	.attr("stroke","lightblue")
    	.attr("stroke-width",resolution/4)
    	.attr("fill","#333333")
    	.style("opacity","0")
    	.style("pointer-events","none");
	new_feature_rect
    	.attr("x",0)
    	.attr("y",0)
   		.attr("width",width)
    	.attr("height",height)
    	.attr("fill","white")
    	.style("opacity","0");
	multi_select_interact_rect
		.attr("x",0)
    	.attr("y",0)
   		.attr("width",width)
    	.attr("height",height)
    	.attr("fill","none")
    	.style("opacity","0");
    	
    //// TIDY THIS AWAY!!!	
	pan_zoom_rect
		.attr("x",0)
    	.attr("y",0)
   		.attr("width",width)
    	.attr("height",height)
    	.attr("fill","white")
    	.style("opacity","0")
    	.style("cursor","pointer")
    	.style("pointer-events","none")
    	.call(d3.behavior.zoom().scaleExtent([0.75, 2]).on("zoom", zoom))
	map_cover
		.attr("x",0)
    	.attr("y",0)
   		.attr("width",width)
    	.attr("height",height)
    	.attr("fill","none")
    	.style("opacity","0.1")
    	.style("cursor","pointer")
    	.on("click",map_view);

	
    	
	//points = main_group.append("g");
		new_feature_group.append("polyline");
		new_feature_group.append("polygon");
		new_feature_group.append("circle");
		//var sel_group = main.append("g");
       //compass_tool = d3.select("#compass_tool")
      	//	.text("off")
      	//	.on("click",function(){if (compass_tool.text() == "off"){compass_tool.text("on"); toggle_compass(1)}else {compass_tool.text("off"); toggle_compass()} });
       
       /*
              <div id="shapes_tool" class="map_tool"></div>
              <div id="compass_tool" class="map_tool"></div>
              <div id="pan_zoom_tool" class="map_tool"></div>
              <div id="select_tool" class="map_tool"></div>
              <div id="gps_tool" class="map_tool"></div>
              <div id="area_select_tool"class="map_tool"></div>
              <div id="overlay_guides_tool" class="map_tool"></div>
       */
       var shapes = ["off"];
       var shape_default;
       if (user_attributes["point"] || user_attributes["point_record"]){shapes.push("point");shape_default = "point"}
       if (user_attributes["line"] || user_attributes["line_record"]){shapes.push("line");if (!shape_default){shape_default = "line"}}
       if (user_attributes["area"] || user_attributes["area_record"]){shapes.push("area");if (!shape_default){shape_default = "area"}}
       confirmation_screen = d3.select("#confirmation_screen").attr("class","conf_hide");
       map_tools = d3.select("#map_tools")
       map_tool_option_shell = d3.select("#map_tool_option_shell");
       current_tool_div = map_tool_option_shell.append("div").attr("id","current_tool").attr("class","hide_current");
       var draw_options = ["edit","delete"];
       var draw_preset;
       if (user_attributes["area"] || user_attributes["area_record"]){draw_options.unshift("area");draw_preset = "area"}
       if (user_attributes["line"] || user_attributes["line_record"]){draw_options.unshift("line");draw_preset = "line"}
       if (user_attributes["point"] || user_attributes["point_record"]){draw_options.unshift("point");draw_preset = "point"}
       draw_tool = new Maptool("draw","drw_",draw_options,draw_preset);
       add_tool(map_tools,map_tool_option_shell,draw_tool);
	   data_tool = new Maptool("data","dat_",["input","sets","style"],"input");
       add_tool(map_tools,map_tool_option_shell,data_tool);
		link_tool = new Maptool("link","lnk_",["join","add","unlink"],undefined);
       add_tool(map_tools,map_tool_option_shell,link_tool);
	   gps_tool = new Maptool("gps","gps_",["on","off","options"],undefined);
       add_tool(map_tools,map_tool_option_shell,gps_tool);
	   manage_tool = new Maptool("manage","man_",["upload","views","reset"],null);
       add_tool(map_tools,map_tool_option_shell,manage_tool);
	   
       
  /*     gps_tool.text("gps").on("click",function(){
      			d3.selectAll(".map_tool_show_options").attr("class","map_tool_options");
      			if (gps_tool.attr("rel") == "on"){
      				gps_tool.attr("rel","off")
      				gps_tool.style("background-color","");
      				getLocation("stop");
      			}
      			else {
      				gps_tool.attr("rel","on")
      				gps_tool.style("background-color","darkseagreen");
      				getLocation("start");
      			}
      		});
      	dotdotdot_tool.text("●●●").on("click",function(){
      			d3.selectAll(".map_tool_show_options").attr("class","map_tool_options");
      			show_more_tools(d3.select(this));
      		});
      	upload_tool.text("upload").on("click",function(){
      			d3.selectAll(".map_tool_show_options").attr("class","map_tool_options");
      			manage_data_upload();
      		});
      	*/
       
       draw_features();
	   


}

function show_more_tools(tool){
	var xpos = tool.property("offsetLeft");
	var screen_width = map_tool_option_shell.property("offsetWidth");
	if (map_tools.style("margin-left") == "0px"){
		map_tools.style("margin-left",screen_width*-0.8+"px");
	}
	else {
		map_tools.style("margin-left","0px");
	}
}

function timestamp(){
	if (!Date.now) {
    	Date.now = function() { return Math.round(new Date().getTime() / 100) };
	}
	return Math.round(Date.now() / 100);
}

function zoom() {
  main_group.attr("transform", "translate(" + d3.event.translate + ")scale(" + d3.event.scale + ")");
  //minimise_options(d3.select("#map_tool_options"));
}

/*      function zoom (ratio){
    	vb_arr[0] = 1*vb_arr[0] + (vb_arr[2] - vb_arr[2]/ratio) / 2;
    	vb_arr[1] = 1*vb_arr[1] + (vb_arr[3] - vb_arr[3]/ratio) / 2;
    	vb_arr[2] = vb_arr[2]/ratio;
    	vb_arr[3] = vb_arr[3]/ratio;
    	svg.transition().delay(200).duration(200).attr("viewBox", vb_arr[0]+" "+vb_arr[1]+" "+vb_arr[2]+" "+vb_arr[3]);
    }*/

/*function toggle_compass(status){
      	// Proxy for compass detection...
      	if (status){
    	  main_group.attr("transform","rotate(45,"+(vb_arr[0]+vb_arr[2]/2)+","+(vb_arr[1]+vb_arr[3]/2)+")");
    	}
    	else {
    	  main_group.attr("transform","rotate(0)");
    	}
      }*/
      
      var drag_node = d3.behavior.drag()
        .on("drag", function(d,i) {
            d[0] += d3.event.dx
            d[1] += d3.event.dy
            d3.select(this)
            	.attr("cx", function(d,i){ return d[0]; })
            	.attr("cy", function(d,i){ return d[1]; })
            draw_nodes();
        });
	var drag_line_node = d3.behavior.drag()
        .on("drag", function(d,i) {
            d[0] += d3.event.dx
            d[1] += d3.event.dy
            d3.select(this)
            	.attr("cx", function(d,i){ return d[0]; })
            	.attr("cy", function(d,i){ return d[1]; })
            draw_line();
    });
      var drag_point = d3.behavior.drag()
        .on("drag", function(d,i) {
            d[0] += d3.event.dx
            d[1] += d3.event.dy
            d3.select(this)
            	.attr("cx", function(d,i){ return d[0]; })
            	.attr("cy", function(d,i){ return d[1]; })
        });


function center_map(x,y,keep){
	svg.attr("viewBox", (x - vb_arr[2]/2)+" "+(y - vb_arr[3]/2)+" "+vb_arr[2]+" "+vb_arr[3]);
	if (keep){
		vb_arr[0] = x - vb_arr[2]/2;
		vb_arr[1] = y - vb_arr[3]/2;
	}
}


      
      function draw_point(){
        var m;
        if (active_feature_id && features.points[active_feature_id]){
        	m = features.points[active_feature_id].points;
        	store_feature(active_feature_id,"points");
        }
        else {
        	m = d3.mouse(this)
        	var my_id = timestamp();
        	features.points[my_id] = {};
        	features.points[my_id]["id"] = my_id;
        	features.points[my_id]["points"] = m;
        	features.points[my_id]["set_id"] = current_set_id;
        	//console.log(m);
        	store_feature(my_id,"points");
        }
        var my_new_point = new_feature_group.selectAll("circle").data([m]);
        my_new_point.attr("cx",function(d){return d[0]}).attr("cy",function(d){return d[1]}).attr("r",resolution).attr("fill","gold").on("click",function(){draw_features()}).call(drag_point).style("pointer-events","auto");
        my_new_point.enter().append("circle").attr("cx",function(d){return d[0]}).attr("cy",function(d){return d[1]}).attr("r",resolution).attr("fill","gold").on("click",function(){draw_features()}).call(drag_point).style("pointer-events","auto");
        draw_features();
      }
      var my_poly_points = new Array;


// Line drawing START
      function start_line(){
        my_poly_points = new Array;
        var m = d3.mouse(this);
        my_poly_points.push(m);
        new_feature_rect.on("click",add_to_line);
        draw_line();
      }
      function add_to_line(){
        var m = d3.mouse(this);
        my_poly_points.push(m);
        draw_line();  
      }
      function finish_line(){
        new_feature_rect.on("click",start_line);
        // add to features  draw properly
        if (active_feature_id){
        	features.lines[active_feature_id].points = my_poly_points.slice(0);
        	store_feature(active_feature_id,"lines");
        }
        else {
        	var my_id = timestamp();
        	features.lines[my_id] = {};
        	features.lines[my_id]["id"] = my_id;
        	features.lines[my_id]["points"] = my_poly_points.slice(0);
        	features.lines[my_id]["set_id"] = current_set_id;
        	store_feature(my_id,"lines");
        }
        draw_features();
        new_feature_group.selectAll("polyline")
        .attr("points","");
        new_feature_group.selectAll("circle")
        .remove();
      }
      function draw_line(){
      	////console.log(my_poly_points);
        var my_new_nodes = new_feature_group.selectAll("circle").data(my_poly_points);
        if (my_poly_points.length > 1){
          new_feature_group.selectAll("polyline")
            .data([my_poly_points])
            .attr("points",function(d) { 
                return d.map(function(d) {
                    return [d[0],d[1]].join(",");
                    }).join(" ");
                })
            .attr("stroke","blue")
            .attr("fill","none")
            .attr("stroke-width",resolution)
            .on("click",add_to_line);
        }
        my_new_nodes.attr("cx",function(d){return d[0]}).attr("cy",function(d){return d[1]}).attr("r",resolution).attr("fill","gold").on("click",finish_line).call(drag_line_node).style("pointer-events","auto");
        my_new_nodes.enter().append("circle").attr("cx",function(d){return d[0]}).attr("cy",function(d){return d[1]}).attr("r",resolution).attr("fill","gold").on("click",finish_line).call(drag_line_node).style("pointer-events","auto");
        my_new_nodes.exit().remove();
      }
// Line drawing END
      
// Area drawing START
      function start_area(){
        my_poly_points = new Array;
        var m = d3.mouse(this);
        my_poly_points.push(m);
        new_feature_rect.on("click",add_to_area);
        draw_nodes();
      }
      function add_to_area(){
        var m = d3.mouse(this);
        my_poly_points.push(m);
        
        draw_nodes();  
      }
      function finish_area(){
        new_feature_rect.on("click",start_area);
        // add to features  draw properly
        if (active_feature_id){
        	features.polygons[active_feature_id].points = my_poly_points.slice(0);
        	////console.log(active_feature_id)
        	store_feature(active_feature_id,"polygons");
        }
        else {
        	var my_id = timestamp();
        	features.polygons[my_id] = {};
        	features.polygons[my_id]["id"] = my_id;
        	features.polygons[my_id]["points"] = my_poly_points.slice(0);
        	features.polygons[my_id]["set_id"] = current_set_id;
        	store_feature(my_id,"polygons");
        }
        draw_features();
        new_feature_group.selectAll("polygon")
        .attr("points","");
        new_feature_group.selectAll("circle")
        .remove();
      }
      function draw_nodes(){
        var my_new_nodes = new_feature_group.selectAll("circle").data(my_poly_points);
        if (my_poly_points.length > 2){
          new_feature_group.selectAll("polygon")
            .data([my_poly_points])
            .attr("points",function(d) { 
                return d.map(function(d) {
                    return [d[0],d[1]].join(",");
                    }).join(" ");
                })
            .attr("stroke","blue")
            .attr("fill","none")
            .attr("stroke-width",resolution)
            .on("click",add_to_area);
        }
        my_new_nodes.attr("cx",function(d){return d[0]}).attr("cy",function(d){return d[1]}).attr("r",resolution).attr("fill","gold").on("click",finish_area).call(drag_node).style("pointer-events","auto");
        my_new_nodes.enter().append("circle").attr("cx",function(d){return d[0]}).attr("cy",function(d){return d[1]}).attr("r",resolution).attr("fill","gold").on("click",finish_area).call(drag_node).style("pointer-events","auto");
        my_new_nodes.exit().remove();
      }
// Area drawing END
	
	function link_selected_features(add){
		// TODO draw lines from feature centroids to selection centroid and fill in feature.links object 
		// Allow links to be deleted and have data associated (but not drawn or edited) in the same way as other shapes
		// Accompany method with unlink to remove links from selected features, recalculating centroid if many items are involved in the group
		// Associate with a links.csv file to accomodate link attributes
		console.log(features.links);
		var link_id;
		var new_link;
		if (add){
			var my_link_id;
			var all_ids = {};
			link_features.forEach(function(feat_id){
				for (my_link_id in geo_features[feat_id].links) {
					if (Object.keys(geo_features[feat_id].links).length == 1){
						all_ids[my_link_id] = 1;
					}
					else {
						link_features.forEach(function(alt_feat_id){
							if (geo_features[feat_id].links[my_link_id] && geo_features[feat_id].links[my_link_id][alt_feat_id]){
  								all_ids[my_link_id] = 1;
  							}
  						});
  					}
  				}
  			});
   			if (Object.keys(all_ids).length == 1){
  				link_id = my_link_id;
        		//unstore_feature(link_id);
        		console.log(link_id);
  				features.links[link_id].forEach(function(feat_id){
  					if (link_features.indexOf(feat_id) < 0){
  						link_features.push(feat_id);
  					}
  				});
  			}
        	else {
        			link_id = timestamp();
        	}
        }
        else {
        	link_id = timestamp();
        	new_link = 1;
        }
  			//	console.log(link_features);
        features.links[link_id] = link_features.slice(0);
        features.links[link_id]["set_id"] = current_set_id;
        link_features.forEach(function(feat_id){
        	link_features.forEach(function(alt_feat_id){
        		if (feat_id != alt_feat_id){
        			//console.log(feat_id+" is linked to "+alt_feat_id);
        			if (!geo_features[feat_id]){
        				geo_features[feat_id] = {}
        			}
        			if (!geo_features[feat_id].links){
        				geo_features[feat_id].links = {}
        			}
        			if (!geo_features[feat_id].links[link_id]){
	        			geo_features[feat_id].links[link_id] = {}
	        		}
	        		geo_features[feat_id].links[link_id][alt_feat_id] = 1;
        		}
        	});
        });
        //console.log(geo_features);
        if (new_link){
        	store_feature(link_id,"links");
        }
        else {
        	update_feature(link_id,"links");
        }
        link_features = [];
        //console.log("!");
        //console.log(link_id);
		multi_features.points = {};
		multi_features.lines = {};
		multi_features.polygons = {};
		draw_features(1);
        draw_features();
	}

	function unlink_selected_features(){
		// TODO allow links to be removed more neatly than just deleted
		link_features.forEach(function(feat_id){
			for (var link_id in geo_features[feat_id].links) {
        		if (geo_features[feat_id].links.hasOwnProperty(link_id)) {
        		
  					link_features.forEach(function(alt_feat_id){
						if (geo_features[feat_id].links[link_id][alt_feat_id]){
  							delete geo_features[feat_id].links[link_id][alt_feat_id];
							
						}
						//if (geo_features[alt_feat_id].links[link_id][feat_id]){
  						//	delete geo_features[alt_feat_id].links[link_id][feat_id];
							
						//}
						//if (Object.keys(geo_features[alt_feat_id].links[link_id]).length == 0){
						//	delete geo_features[alt_feat_id].links[link_id];
						//}
						if (Object.keys(geo_features[feat_id].links[link_id]).length == 0){
							delete geo_features[feat_id].links[link_id];
						}
					});	
						
					if (features.links[link_id] ){
						features.links[link_id].splice(features.links[link_id].indexOf(feat_id),1);
						if (features.links[link_id].length <= 1){
							delete features.links[link_id];
							//delete geo_features[feat_id].links[link_id];
							unstore_feature(link_id);
  					
						}
						else {
		        			update_feature(link_id,"links");
		        		}
					}
				}
			}
				
        });
        	link_features = [];
		multi_features.points = {};
			multi_features.lines = {};
			multi_features.polygons = {};
			draw_features(1);
		draw_features();
	}
	
	function attach_record_count (my_group,feature_id,count){
		count_group = my_group.append("g").attr("id",feature_id+"_count").style("pointer-events","none");
		count_group.attr("transform","translate("+(geo_features[feature_id].x+2*resolution/3)+","+(geo_features[feature_id].y-2*resolution/3)+")")
        count_group.append("circle").style("fill","#333333").style("stroke","white").attr("r",2*resolution/5);
        count_group.append("text").style("fill","white").text(count).style("text-anchor","middle").style("font-weight","bold").style("font-size","150%").attr("dy",resolution/5);
        
	}
	function attach_attr_value (my_group,feature_id,str){
		count_group = my_group.append("g").attr("id",feature_id+"_string").style("pointer-events","none");
		count_group.attr("transform","translate("+(geo_features[feature_id].x+2*resolution/3)+","+(geo_features[feature_id].y-2*resolution/3)+")")
        count_group.append("ellipse").style("fill","white").style("stroke","#333333").attr("ry",2*resolution/5).attr("rx",function(){var scale = 2*resolution/5; if (str.length > 2){return scale * ((str.length+1) / 2)} return scale});
        count_group.append("text").style("fill","#333333").text(str).style("text-anchor","middle").style("font-weight","bold").style("font-size","150%").attr("dy",resolution/5);
        
	}
	
	
    function draw_features(where){
    	if (where){
    		multi_polygon_group.selectAll("polygon").remove();
          	for (var key in multi_features.polygons) {
  				if (multi_features.polygons.hasOwnProperty(key)) {
  					////console.log(features.polygons[key]);
  					multi_polygon_group.append("polygon")
  					.data([multi_features.polygons[key]])
    				.attr("points",function(d) { 
                	return d.points.map(function(d) {////console.log(d);
                    	return [d[0],d[1]].join(",");
                    	}).join(" ");
                	})
            		.attr("fill", "gold")
            		.style("opacity","0.6")
            		.attr("stroke-width",resolution)
            		.attr("rel",key)
            		.on("click",function(d,i){
            				delete multi_features.polygons[d3.select(this).attr("rel")];
            				draw_features(1);
            			});
            	}
  			}
  			
		  	multi_point_group.selectAll("circle").remove();
          	for (var key in multi_features.points) {
  				if (multi_features.points.hasOwnProperty(key)) {
  					multi_point_group.append("circle")
  					.data([multi_features.points[key]])
    				.attr("cx",function(d) { return d.points[0] })
            		.attr("cy",function(d) { return d.points[1] })
            		.attr("r",resolution)
            		.attr("fill","gold")
            		.style("opacity","0.6")
            		.attr("stroke-width",resolution)
            		.attr("rel",key)
            		.on("click",function(d,i){
            				delete multi_features.points[d3.select(this).attr("rel")];
            				//console.log(key);
            				draw_features(1);
          				});
  				}
		  	}
		  	multi_line_group.selectAll("polyline").remove();
          	for (var key in multi_features.lines) {
  				if (multi_features.lines.hasOwnProperty(key)) {
  					////console.log(key);
  					multi_line_group.append("polyline")
  					.data([multi_features.lines[key]])
    				.attr("points",function(d) { 
                	return d.points.map(function(d) {////console.log(d);
                    	return [d[0],d[1]].join(",");
                    	}).join(" ");
                	})
            		.attr("stroke", "gold")
	            	.attr("fill","none")
            		.style("opacity","0.6")
            		.attr("stroke-width",resolution)
            		.attr("rel",key)
            		.on("click",function(d,i){
            				delete multi_features.lines[d3.select(this).attr("rel")];
            				draw_features(1);
          				});
  				}
		  	}
    	
    	
    	}
    	else {
          ////console.log(features);
          active_feature_id = undefined;
          polygon_group.selectAll("polygon").remove();
          polygon_group.selectAll("g").remove();
          for (var key in features.polygons) {
  			if (features.polygons.hasOwnProperty(key)) {
  				if (!features.polygons[key].hidden){
  				////console.log(features.polygons[key]);
  				polygon_group.append("polygon")
  				.data([features.polygons[key]])
    			.attr("points",function(d) { 
                return d.points.map(function(d) {////console.log(d);
                    return [d[0],d[1]].join(",");
                    }).join(" ");
                })
            	.attr("fill", function (d){
            		if (geo_features[d.id] && geo_features[d.id].records.length > 0){
            			return "green";
            		}
            		return "red";
	            })
            	.style("opacity","0.6")
            	.attr("stroke-width",resolution)
            	.on("click",function(d,i){
            		my_poly_points = d.points;
            		active_feature_id = d.id;
            		if (!current_tool){
            			multi_features.polygons[d.id] = features.polygons[d.id];
            			link_features.push(d.id)
            			draw_features(1);
            		}
            		else if(current_tool == "delete"){
            			draw_nodes();
            			confirm_delete(geo_features[d.id].records,features.polygons,d.id);
            		}
            		else {
            			if(current_tool == "input"){
            				record_view()
            			}
            			if(current_tool == "edit"){
							new_feature_group.selectAll("circle").remove();
							new_feature_group.selectAll("polygon").attr("points","");
							new_feature_group.selectAll("polyline").attr("points","");
            			}
            			draw_nodes()
            		}
            	})
            	.attr("rel",function(d){
            		var my_x = 0;
            		var my_y = 0;
            		d.points.forEach(function (vertice){
            				my_x += vertice[0];
            				my_y += vertice[1];
            			});
            			my_x /= d.points.length;
            			my_y /= d.points.length;
	            	if (geo_features[d.id]){
	            		// Just update E and N
	            		geo_features[d.id].x = my_x;
	            		geo_features[d.id].y = my_y;
	            		geo_features[d.id].easting = my_x;
	            		geo_features[d.id].northing = height - my_y;
	            	}
	            	else {
          				geo_features[d.id] = {};
          				geo_features[d.id].feature_id = d.id;
          				geo_features[d.id].feature_type = "area";
	            		geo_features[d.id].x = my_x;
	            		geo_features[d.id].y = my_y;
	            		geo_features[d.id].easting = my_x;
	            		geo_features[d.id].northing = height - my_y;
          				geo_features[d.id].records = [];
          			}
          			if (active_records[geo_features[d.id].records[0]]){
          				if (geo_features["area"].show){
          					var text = active_records[geo_features[d.id].records[0]][geo_features["area"].show];
          					attach_record_count(polygon_group,d.id,text);
          				}
          				else if (user_attributes[geo_features[d.id].feature_type+"_record"]){
          					var count = geo_features[d.id].records.length;
          					if (user_attributes[geo_features[d.id].feature_type]){
          						count -= 1;
          					}
          					if (count > 0){
          						attach_attr_value(polygon_group,d.id,count);
          					}
          				}
          			}
            	});
            	}
  			}
  			
  			////console.log(active_feature_id)
		  }
		  point_group.selectAll("circle").remove();
		  point_group.selectAll("g").remove();
          for (var key in features.points) {
  			if (features.points.hasOwnProperty(key)) {
  				//console.log(key);
  				if (!features.points[key].hidden){
  				point_group.append("circle")
  				.data([features.points[key]])
    			.attr("cx",function(d) { return d.points[0] })
            	.attr("cy",function(d) { return d.points[1] })
            	.attr("r",resolution)
            	.attr("class", function (d){
            		if (geo_features[d.id] && geo_features[d.id].records.length > 0){
            			if (geo_features["point"].style_by){
            				console.log("attempting to style point by "+geo_features["point"].style_by);
            				return geo_features["point"].style_prefix+active_records[geo_features[d.id].records[0]][geo_features["point"].style_by];
            			}
            			else {
	            			return "good_point";
	            		}
            		}
            		return "point";
	            })
            	.style("opacity","0.6")
            	.attr("stroke-width",resolution)
            	.on("click",function(d,i){
            		active_feature_id = d.id;
            		if (!current_tool){
            			multi_features.points[d.id] = features.points[d.id];
            			link_features.push(d.id)
            			draw_features(1);
            		}
            		else if(current_tool == "delete"){
            			draw_point();
            			confirm_delete(geo_features[d.id].records,features.points,d.id);
            		}
            		else {
            			if(current_tool == "input"){
            				record_view();
            			}
            			if(current_tool == "edit"){
							new_feature_group.selectAll("circle").remove();
							new_feature_group.selectAll("polygon").attr("points","");
							new_feature_group.selectAll("polyline").attr("points","");
            			}
            			draw_point();
            		}
            	})
            	.attr("rel",function(d){
	            	if (geo_features[d.id]){
	            		// Just update E and N
	            		geo_features[d.id].x = d.points[0];
	            		geo_features[d.id].y = d.points[1];
	            		geo_features[d.id].easting = d.points[0];
	            		geo_features[d.id].northing = height - d.points[1];
	            	}
	            	else {
          				geo_features[d.id] = {};
          				geo_features[d.id].feature_id = d.id;
          				geo_features[d.id].feature_type = "point";
	            		geo_features[d.id].x = d.points[0];
	            		geo_features[d.id].y = d.points[1];
	            		geo_features[d.id].easting = d.points[0];
	            		geo_features[d.id].northing = height - d.points[1];
          				geo_features[d.id].records = [];
          			}
          			if (active_records[geo_features[d.id].records[0]]){
          				if (geo_features["point"].show){
          					var text = active_records[geo_features[d.id].records[0]][geo_features["point"].show];
          					attach_attr_value(point_group,d.id,text);
          				}
          				else if (user_attributes[geo_features[d.id].feature_type+"_record"]){
          					var count = geo_features[d.id].records.length;
          					if (user_attributes[geo_features[d.id].feature_type]){
          						count -= 1;
          					}
          					if (count > 0){
          						attach_record_count(point_group,d.id,count);
          					}
          				}
          			}
          		})
          		}
  			}
		  }
		  line_group.selectAll("polyline").remove();
		  line_group.selectAll("g").remove();
          for (var key in features.lines) {
  			if (features.lines.hasOwnProperty(key)) {
  				if (!features.lines[key].hidden){
  			////console.log(key);
  				line_group.append("polyline")
  				.data([features.lines[key]])
    			.attr("points",function(d) { 
                return d.points.map(function(d) {////console.log(d);
                    return [d[0],d[1]].join(",");
                    }).join(" ");
                })
            	.attr("stroke", function (d){
            		if (geo_features[d.id] && geo_features[d.id].records.length > 0){
            			return "green";
            		}
            		return "red";
	            })
	            .attr("fill","none")
            	.style("opacity","0.6")
            	.attr("stroke-width",resolution)
            	.on("click",function(d,i){
            		my_poly_points = d.points;
            		active_feature_id = d.id;
            		if (!current_tool){
            			multi_features.lines[d.id] = features.lines[d.id];
            			link_features.push(d.id)
            			draw_features(1);
            		}
            		else if(current_tool == "delete"){
            			draw_line();
            			confirm_delete(geo_features[d.id].records,features.lines,d.id);
            		}
            		else {
            			if(current_tool == "input"){
            				record_view();
            			}
            			if(current_tool == "edit"){
							new_feature_group.selectAll("circle").remove();
							new_feature_group.selectAll("polygon").attr("points","");
							new_feature_group.selectAll("polyline").attr("points","");
            			}
            			draw_line();
            		}
            	})
            	.attr("rel",function(d){
            		var my_x = 0;
            		var my_y = 0;
            		if (d.points.length % 2 == 0){
	            		var min = Math.floor((d.points.length -1) / 2);
    	        		var max = min + 1;
        	    		//console.log(min);
            			my_x = (d.points[min][0] + d.points[max][0])/2;
            			my_y = (d.points[min][1] + d.points[max][1])/2;
            		}
            		else {
            			var mid = Math.floor((d.points.length -1) / 2);
            			//console.log(mid);
            			
            			my_x = d.points[mid][0];
            			my_y = d.points[mid][1];
            		}
	            	if (geo_features[d.id]){
	            		// Just update E and N
	            		geo_features[d.id].x = my_x;
	            		geo_features[d.id].y = my_y;
	            		geo_features[d.id].easting = my_x;
	            		geo_features[d.id].northing = height - my_y;
	            	}
	            	else {
          				geo_features[d.id] = {};
          				geo_features[d.id].feature_id = d.id;
          				geo_features[d.id].feature_type = "line";
	            		geo_features[d.id].x = my_x;
	            		geo_features[d.id].y = my_y;
	            		geo_features[d.id].easting = my_x;
	            		geo_features[d.id].northing = height - my_y;
          				geo_features[d.id].records = [];
          			}
          			if (active_records[geo_features[d.id].records[0]]){
          				if (geo_features["line"].show){
          					var text = active_records[geo_features[d.id].records[0]][geo_features["line"].show];
          					attach_attr_value(line_group,d.id,text);
          				}
          				else if (user_attributes[geo_features[d.id].feature_type+"_record"]){
          					var count = geo_features[d.id].records.length;
          					if (user_attributes[geo_features[d.id].feature_type]){
          						count -= 1;
          					}
          					if (count > 0){
          						attach_record_count(line_group,d.id,count);
          					}
          				}
          			}
          		});
          		}
  			}
		  }
          
          link_group.selectAll("path").remove();
          link_group.selectAll("g").remove();
          
          for (var link_id in features.links) {
  			if (features.links.hasOwnProperty(link_id)) {
  				//console.log(features.links[link_id]);
  				if (!features.links[link_id].hidden){
  				var data = {"points":[]};
        		data["id"] = link_id;
        		var my_x = 0;
            	var my_y = 0;
            	features.links[link_id].forEach(function(key){
        			data["points"].push([geo_features[key].x,geo_features[key].y]);
        			//store_feature(link_id,"links");
            		my_x += geo_features[key].x;
            		my_y += geo_features[key].y;
        		});
  				my_x /= data["points"].length;
            	my_y /= data["points"].length;
  				link_group.append("path")
  				.data([data])
    			.attr("d",function(d) { 
                return d.points.map(function(d) {////console.log(d);
                    return ["M"+my_x,my_y].join(",") + [" L"+d[0],d[1]].join(",");
                    }).join(" ");
                })
            	.attr("class", function (d){
            		if (geo_features[d.id] && geo_features[d.id].records.length > 0){
            			if (geo_features["link"].style_by){
            				console.log("attempting to style link by "+geo_features["link"].style_by);
            				return geo_features["link"].style_prefix+active_records[geo_features[d.id].records[0]][geo_features["link"].style_by];
            			}
            			else {
	            			return "good_link";
	            		}
            		}
            		return "link";
	            })
            	.attr("fill","none")
            	.style("opacity","0.6")
            	.attr("stroke-width",resolution)
            	.on("click",function(d,i){
            		//my_poly_points = d.points;
            		active_feature_id = d.id;
            		//if(current_tool == "delete"){
            		//	draw_nodes();
            		//	confirm_delete(geo_features[d.id].records,features.polygons,d.id);
            		//}
            		//else if (current_tool == "select"){
            		//	multi_features.polygons[d.id] = features.polygons[d.id];
            		//	draw_features(1);
            		//}
            		//else {
            			if(current_tool == "input"){
            				record_view()
            			}
            			//if(current_tool == "edit"){
						//	new_feature_group.selectAll("circle").remove();
						//	new_feature_group.selectAll("polygon").attr("points","");
						//	new_feature_group.selectAll("polyline").attr("points","");
            			//}
            			//draw_nodes()
            		//}
            	})
            	.attr("rel",function(d){
            		if (geo_features[d.id]){
	            		// Just update E and N
	            		geo_features[d.id].x = my_x;
	            		geo_features[d.id].y = my_y;
	            		geo_features[d.id].easting = my_x;
	            		geo_features[d.id].northing = height - my_y;
	            	}
	            	else {
          				geo_features[d.id] = {};
          				geo_features[d.id].feature_id = d.id;
          				geo_features[d.id].feature_type = "link";
	            		geo_features[d.id].x = my_x;
	            		geo_features[d.id].y = my_y;
	            		geo_features[d.id].easting = my_x;
	            		geo_features[d.id].northing = height - my_y;
          				geo_features[d.id].records = [];
          			}
            	});
            	}
  			}
  			////console.log(active_feature_id)
		  }
          
      }
    }