      var record_id = 0;
      var next_record_id;
      var default_delay = 1000;
  
  
    
      //show_feature_detail(active_feature_id);
      function show_feature_detail(active_feature_id){
      	type = geo_features[active_feature_id].feature_type;
      	var user_feature_container = d3.select("#key_info").html("");
      	
      	user_feature_container.append("div").attr("id","active_feature_id").attr("class","feature_value").text(type+": ");
      	user_feature_container.append("div").attr("id","feature_gridref").attr("class","feature_value").text(feature_gridref(active_feature_id,6));
      	var record_nav = user_feature_container.append("div").attr("id","record_nav");
      	var previous_record = record_nav.append("div").attr("id","record_nav_previous").attr("class","record_nav_control");
      	var current_record = record_nav.append("div").attr("id","record_nav_current").attr("class","record_nav_control");
      	var next_record = record_nav.append("div").attr("id","record_nav_next").attr("class","record_nav_control");
      	var add_record = record_nav.append("div").attr("id","record_nav_add").attr("class","record_nav_control");
      	if (type.match("_record") || user_attributes[type+"_record"]){
      		add_record.text("+").on("click",function(){create_new_record(active_feature_id)});
      	}
      	else {
      		add_record.html("&nbsp;");
      	}
      	for(var r = 0; r < geo_features[active_feature_id]["records"].length; r++){
          if (r == 0){
            record_id = geo_features[active_feature_id]["records"][0];
            display_record_attributes(record_id,active_feature_id);
            current_record.text((r+1)+"/"+geo_features[active_feature_id]["records"].length);
            next_record.html("&nbsp;")
            previous_record.html("&nbsp;")
          }
          else if (r == 1){
          	record_id = geo_features[active_feature_id]["records"][r];
          	next_record.html("▶").on("click",function(){update_record_nav(active_feature_id,1); display_record_attributes(record_id,active_feature_id)});
          	previous_record.html("&nbsp;")
          }
      	}
      }
      function create_new_record(active_feature_id){
      	type = geo_features[active_feature_id].feature_type;
      	if (!user_attributes[type]){
      		type = geo_features[active_feature_id].feature_type + "_record";
      	}
      	else if (!type.match("_record") && geo_features[active_feature_id].records.length > 0){
      		type = geo_features[active_feature_id].feature_type + "_record";
      	}
      	next_record_id = timestamp();
        active_records[next_record_id] = {"record_id":next_record_id,"feature_id":active_feature_id,"feature_type":type};
        active_records[next_record_id];
        for(var i = 0; i < user_attributes[type].length; i++){
        	console.log(attribute_object[type][user_attributes[type][i]].lookup)
          if (attribute_object[type][user_attributes[type][i]].lookup == "index"){
            active_records[next_record_id][user_attributes[type][i]] = attribute_object[type][user_attributes[type][i]]["userdefault"];
          }
          if (attribute_object[type][user_attributes[type][i]].lookup == "gridref"){
            active_records[next_record_id][user_attributes[type][i]] = feature_gridref(active_feature_id,attribute_object[type][user_attributes[type][i]]["userdefault"]);
            attribute_object[type][user_attributes[type][i]].values = attribute_object[type][user_attributes[type][i]].values.slice(0,1);
            attribute_object[type][user_attributes[type][i]].values.push(feature_gridref(active_feature_id,10));
            attribute_object[type][user_attributes[type][i]].values.push(feature_gridref(active_feature_id,8));
            attribute_object[type][user_attributes[type][i]].values.push(feature_gridref(active_feature_id,6));
            attribute_object[type][user_attributes[type][i]].values.push(feature_gridref(active_feature_id,4));
          }
          else if (attribute_object[type][user_attributes[type][i]].lookup == "function"){
           	var val = ""+attribute_object[type][user_attributes[type][i]]["userdefault"];
           	val = string_to_function(val.replace(/\(.*\)/,""));
			active_records[next_record_id][user_attributes[type][i]] = val;
          }
          else {
           	active_records[next_record_id][user_attributes[type][i]] = attribute_object[type][user_attributes[type][i]]["userdefault"];
          }
        }
        geo_features[active_feature_id]["records"].push(next_record_id)
      	update_record_nav(active_feature_id,(geo_features[active_feature_id]["records"].length-1)); 
      	display_record_attributes(next_record_id,active_feature_id)
      }
      
      function update_record_nav(active_feature_id,r){
      	if (r > 0){
          d3.select("#record_nav_previous").html("◀").on("click",function(){update_record_nav(active_feature_id,(r-1)); display_record_attributes(geo_features[active_feature_id]["records"][(r-1)],active_feature_id)});
        }
        else {
          d3.select("#record_nav_previous").html("&nbsp;")
        }
        if (r + 1 <  geo_features[active_feature_id]["records"].length){
          d3.select("#record_nav_next").html("▶").on("click",function(){update_record_nav(active_feature_id,(r+1)); display_record_attributes(geo_features[active_feature_id]["records"][(r+1)],active_feature_id)});
        }
        else {
          d3.select("#record_nav_next").html("&nbsp;")
        }
        d3.select("#record_nav_current").text((r+1)+"/"+geo_features[active_feature_id]["records"].length);
      }
      
	  function feature_gridref(id,num) {
	  	var gridref = new OsGridRef(geo_features[id].easting, geo_features[id].northing);
	  	return gridref.toString(num);
	  }
      
      function make_default(el,att,val){
      	var user_attribute_header = el;
      	//user_attribute_header.style("background-color","steelblue");
      	user_attribute_header.selectAll(".attribute_set_default").text("▣");
      	attribute_object[type][att].userdefault = val;
      	
      	localStorage.setItem("attribute_object",JSON.stringify(attribute_object));
      	
      }
      function display_record_attributes(record_id,active_feature_id){
      	type = geo_features[active_feature_id].feature_type;
      	if (!type.match("_record") && (geo_features[active_feature_id].records[0] != record_id || !attribute_object[type])){
      		type = geo_features[active_feature_id].feature_type + "_record";
      	}
    	var user_attribute_data = d3.select("#attribute_container").html("").selectAll("div").data(user_attributes[type]);
       user_attribute_data
        		.enter()
        		.call(function(d,i){ var what = d; display_each_attribute(d3.select("#attribute_container"),what,record_id,active_feature_id) });
        		return;
        	
        	
																		
        user_attribute_data.exit().remove();
        // add image here
        user_attribute_data = d3.select("#attribute_container");
        var user_attribute_shell = user_attribute_data 
        		.append("div")
        		.attr("class","attribute_shell collapsed")
        		.attr("id","record_image");
        var user_attribute_header = user_attribute_shell.append("div").attr("class","attribute_header").attr("rel","0").on("click",function(){toggle_attribute("record_image")});
        
        var user_attribute_text_div = user_attribute_header.append("div").attr("class","attribute_text");
        user_attribute_text_div.append("div").attr("class","attribute_type").text("image");
        user_attribute_text_div
        	.append("div")
        	.attr("class","attribute_current_value")
        	.text("");
        image_output = user_attribute_shell
        	.append("div").attr("class","attribute_image centered");
        image = image_output.append("img")
        	.style("max-width","25%");
        //image_save = image_output.append("a")
    		//.attr("id","save")
    		//.attr("download","earth.txt")
    		//.attr("target","_blank")
    		//.text("Save");
    	image_input = image_output.append("input");
        image_input
        	.attr("class","image_input")
        	.attr("rel",function(){var my_attribute = "record_image"; return my_attribute;})
			.attr("type","file")
			.attr("capture","camera") 
			.attr("accept","image/*")
			.attr("id","cameraInput")
			.attr("name","cameraInput")
			.on("change",function(){handleFiles(this)});
		

      }
      
	var display_each_attribute = function (my_div,what,record_id,active_feature_id){
      	what[0].forEach(function(datum,index){
    		var att_name = datum.__data__;
    		my_shell = my_div.append("div")
        		.attr("class",function(){ var my_class = "attribute_shell "+ attribute_object[type][att_name]["css_class"]; 
        				// Sequential attributes
        				if(att_name.match(/[^\d]\d+$/)){ 
        					var index = att_name.replace(/^.*[^\d]+/,"");
        					index = index*1 - 1;
        					var stub = att_name.replace(/\d+$/,"");
        					var prev = stub+index;
        					if (active_records[record_id][prev] && (active_records[record_id][prev] == 0  || active_records[record_id][prev] == "-")){
        						my_class = "hidden_shell";
        					}
        				}
        				// Conditional attributes
        				if (attribute_object[type][att_name].conditional_on){
        					var match = 0;
        					attribute_object[type][att_name].conditional_on.forEach(function(cond_attr,cond_index){
        						if (active_records[record_id][cond_attr] == attribute_object[type][att_name].conditional_value[cond_index]){
        							match = 1;
        						}
        					});
        					if (match == 0){
        						my_class = "hidden_shell";
        					}
        				}
        				return my_class 
        			})
        		.attr("id",function(){ return "attribute_"+index })
        		.attr("rel",function(){ return att_name })
     		
     		var user_attribute_header = my_shell.append("div").attr("class","attribute_header")
        		.attr("rel",function(){ if (attribute_object[type][att_name]["css_class"] == "collapsed"){return "0"}return "1" })
        		.on("click",function(){var attribute_id = "attribute_"+index; toggle_attribute(attribute_id)})
        	
        	
       
       	 	user_attribute_header.append("div").attr("class","attribute_set_default").text(function(){
        		if (attribute_object[type][att_name].userdefault == active_records[record_id][user_attributes[type][index]]){
        			return "▣";
        		}return "▢"} )
        		.on("click",function(){
        			d3.event.stopPropagation();
        			var el = d3.select(this.parentNode);
        			var val; 
        			val = active_records[record_id][user_attributes[type][index]];
        			if (attribute_object[type][user_attributes[type][index]].lookup == "gridref" && val.length > 1){
            			val = val.length - 4
          			}
        			make_default(user_attribute_header,att_name,val); 
        		});
        		var user_attribute_text_div = user_attribute_header.append("div").attr("class","attribute_text");
        		user_attribute_text_div.append("div").attr("class","attribute_type").text(att_name);
        		user_attribute_text_div
        			.append("div")
        			.attr("class","attribute_current_value")
        			.text(function(){ 
        				if (attribute_object[type][user_attributes[type][index]].lookup == "index"){
        					return attribute_object[type][att_name].values[active_records[record_id][user_attributes[type][index]]];
        				}
        				else {
        					return active_records[record_id][user_attributes[type][index]];
        				}
        			});
      			fit_current(user_attribute_header);
      			
      		var user_attribute_options = my_shell
        		.append("div")
        		.attr("class","attribute_body")
        		.selectAll(".attribute_option")
        		.data(function(){
        			if (attribute_object[type][user_attributes[type][index]].lookup == "gridref"){
        				attribute_object[type][user_attributes[type][index]].values = attribute_object[type][user_attributes[type][index]].values.slice(0,1);
            			attribute_object[type][user_attributes[type][index]].values.push(feature_gridref(active_feature_id,10));
            			attribute_object[type][user_attributes[type][index]].values.push(feature_gridref(active_feature_id,8));
            			attribute_object[type][user_attributes[type][index]].values.push(feature_gridref(active_feature_id,6));
            			attribute_object[type][user_attributes[type][index]].values.push(feature_gridref(active_feature_id,4));
        			}
        			return attribute_object[type][user_attributes[type][index]].values
        		});
        	user_attribute_options
        		.enter()
        		.append("div")
        		.attr("class",function(d,i){ 
        			var my_class = "attribute_option"; 
        			if (attribute_object[type][att_name].lookup == "index" && i == active_records[record_id][att_name]){
        				my_class += " attribute_selected"
        			}
        			else if (attribute_object[type][att_name].lookup == "value" && d == active_records[record_id][att_name]){
        				my_class += " attribute_selected"
        			}
        			return my_class; 
        		})
        		.text(function(d,i){ 
        			var val = ""+d;
          			if (val.match("\w\\(.*\\)")){ 
          				val = string_to_function(d.replace(/\(.*\)/,""));
					}
					return val;
        		
        		})
        		.on("click",function(d,i){ 
        				if (attribute_object[type][att_name].lookup == "index"){ 
        				  active_records[record_id][att_name] = i;
        				}
        				else {
        				  active_records[record_id][att_name] = d;
        				}
        				// Linked attributes
        				if (attribute_object[type][att_name].linked_to){
        					var linked = attribute_object[type][att_name].linked_to.split(";");
        					linked.forEach(function(link_attr){
        						active_records[record_id][link_attr] = attribute_object[type][link_attr].values[i];
	        				});
	        			}
        				
        				display_record_attributes(record_id,active_feature_id) });
        	//user_attribute_options.exit().remove();
        	var my_sel = my_shell.selectAll(".attribute_selected")
      	  	if (!my_sel.empty()){
      	  		var position = my_sel.property("offsetTop") - my_sel.property("offsetHeight");
      	    	my_shell.selectAll(".attribute_body").property("scrollTop",position)
      	  	}
      	  
      		
      	  	if (attribute_object[type][att_name].placeholder){
      	  	
      			// Add ability to choose from suggested values when typing
      			if (attribute_object[type][att_name].options){
					my_shell
        			.append("div")
        				.attr("class","suggest_list")
        				.attr("id",att_name+"_suggest_list")
        			//user_attribute_suggest.text(attribute_object[type][att_name].options[0]);
        		}
		        
      			
      	  		var user_attribute_enter = my_shell
        			.append("div").attr("class","attribute_option centered");
        		
      			user_attribute_enter
		        	.append("input")
		        	.attr("class","attribute_input")
		        	.style("height",function(){var my_attribute = user_attributes[type][d3.select(this.parentNode.parentNode).attr("id").replace("attribute_","")]; if (attribute_object[type][my_attribute].placeholder){return ""}return "0px"})
		        	.attr("rel",function(){var my_attribute = user_attributes[type][d3.select(this.parentNode.parentNode).attr("id").replace("attribute_","")]; return my_attribute;})
					.attr("type",function(){var my_attribute = d3.select(this).attr("rel"); return attribute_object[type][my_attribute].type})
		        	.attr("placeholder",function(){var my_attribute = d3.select(this).attr("rel"); return attribute_object[type][my_attribute].placeholder})
        			.on("keyup",function(){
							var tb = d3.select(this);
							var local_att = tb.attr("rel");
							var local_val = this.value;
							var new_entry = d3.select("#"+local_att+"_new_entry");
							var suggest_list;
							if (attribute_object[type][att_name].options){
								suggest_list = d3.select("#"+local_att+"_suggest_list");
							}
							if (suggest_list){
								if(!local_val || local_val.length < 1){
									suggest_list.html("")
								}
								else {
									set_suggestions(suggest_list,local_val,attribute_object[type][att_name].options,local_att,record_id,active_feature_id);
								}
							}
							if(!local_val){new_entry.text("");return;}
							show_new_entry(new_entry,local_val,local_att,record_id,active_feature_id);
							
							
						});
				user_attribute_enter
        			.append("div").attr("class","new_entry")
        			.attr("id",att_name+"_new_entry")
      			}
      		});
    }
      
     function show_new_entry(my_div,string,local_att,record_id,active_feature_id){
     		my_div.text(string);
			if ((attribute_object[type][local_att].type == "text" || attribute_object[type][local_att].type == "number") && !string.match(attribute_object[type][local_att].test)){
				my_div.style("background-color","#ffcccc");
			}
			else {
				my_div.style("background-color","#ffffff");
			}
			my_div.on("click",function(){
				if ((attribute_object[type][local_att].type == "text" || attribute_object[type][local_att].type == "number") && !string.match(attribute_object[type][local_att].test)){
					return;
				}
				attribute_object[type][local_att].values.push(string);
				if (attribute_object[type][local_att].lookup == "index"){ 
		          active_records[record_id][local_att] = attribute_object[type][local_att].values.length - 1;
		        }
		        else {
		          active_records[record_id][local_att] = string;
		        }
		       if (attribute_object[type][local_att].limit && attribute_object[type][local_att].values.length >= attribute_object[type][local_att].limit){
					tmp_arr = attribute_object[type][local_att].values.slice(0);
					tmp_arr = tmp_arr.slice(0,1);
					attribute_object[type][local_att].values.length
					attribute_object[type][local_att].values = tmp_arr.concat(attribute_object[type][local_att].values.slice(attribute_object[type][local_att].values.length-attribute_object[type][local_att].limit+1));
				}
				localStorage.setItem("attribute_object",JSON.stringify(attribute_object));
				display_record_attributes(record_id,active_feature_id);
			});
      }
      
      function set_suggestions(my_div,string,options,local_att,record_id,active_feature_id){
     		my_div.html("");
     		var regex = new RegExp(string, "i");
     		var ctr = 0;
     		var max = 25;   /////////// TODO add option to setup.csv to control this value
      		options.forEach(function(option,index){
      			if (option.match(regex)){
      				ctr++;
      				if (ctr > max){return}
      				my_div.append("div").html(option.replace(regex,"<b>"+string+"</b>"))
      				.on("click",function(){
      					// should also remove from options list
      					attribute_object[type][local_att].values.push(option);
						if (attribute_object[type][local_att].lookup == "index"){ 
		          			active_records[record_id][local_att] = attribute_object[type][local_att].values.length - 1;
		        		}
		        		else {
		          			active_records[record_id][local_att] = option;
		        		}
		        		
		        		// Linked attributes
        				if (attribute_object[type][local_att].linked_to){
        					var linked = attribute_object[type][local_att].linked_to.split(";");
        					linked.forEach(function(link_attr){
        						attribute_object[type][link_attr].values.push(attribute_object[type][link_attr].options[index]);
        						active_records[record_id][link_attr] = attribute_object[type][link_attr].options[index];
	        				});
	        			}	
	        				
		        		
		       			localStorage.setItem("attribute_object",JSON.stringify(attribute_object));
						display_record_attributes(record_id,active_feature_id);
      				})
      			}
      		});
      }
      
      
      function handleFiles(iFile) {
            var path = iFile.value;
            console.log(iFile);
            console.log(path);
            //var reader = new FileReader();
            //img.attr("src",path);
            //reader.readAsDataURL(path);
            var reader = new FileReader();
        reader.onload = (function(e) {
            image.attr("src",e.target.result);
            //console.log(image.attr("src"));
            //image_save.attr("href","data:text/plain,{image:"+e.target.result+"}&#10;");
          }
        );
        reader.readAsDataURL(document.getElementById("cameraInput").files[0]);
  }

      function toggle_attribute(attribute_id){
      	var attribute_shell = d3.select("#"+attribute_id);
      	var attribute = user_attributes[type][attribute_id.replace("attribute_","")];
      	var toggle_div = attribute_shell.selectAll(".attribute_header");
      	var current = toggle_div.attr("rel");
      	if (current == "0"){
      	  attribute_shell.attr("class","attribute_shell expanded");
      	  // attribute_object[type][attribute]["css_class"] = "expanded"; // uncomment to make expansion setting persistent
      	  var my_sel = attribute_shell.selectAll(".attribute_selected")
      	  if (!my_sel.empty()){
      	  var position = my_sel.property("offsetTop") - my_sel.property("offsetHeight");
      	    attribute_shell.selectAll(".attribute_body").property("scrollTop",position)
      	  }
      	  toggle_div.attr("rel","1")
      	}
      	else {
      	  attribute_shell.attr("class","attribute_shell collapsed");
      	  // attribute_object[type][attribute][0]["css_class"] = "collapsed"; // uncomment to make expansion setting persistent
      	  toggle_div.attr("rel","0")
        }
      }
      
function fit_current(div){
	value_div = div.selectAll(".attribute_current_value");
	toggle_div = div.selectAll(".attribute_set_default");
	if(value_div.node().getBoundingClientRect().right > toggle_div.node().getBoundingClientRect().left){
		font_size = value_div.style("font-size")
		current = font_size.replace("px","")
		current--;
		value_div.style("font-size",current+"px")
		fit_current(div);
	}
}

